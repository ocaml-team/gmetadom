<?xml version="1.0"?>

<!--
 This file is part of MetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the author's home page
 http://www.cs.unibo.it/~lpadovan
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>

<xsl:output method="text"/>
<xsl:param name="module" select="'Core'"/>
<xsl:param name="uriprefix" select="''"/>

<xsl:template match="interface">
<xsl:variable name="prefix">
  <xsl:call-template name="gdomePrefixOfType">
    <xsl:with-param name="type" select="@name"/>
  </xsl:call-template>
</xsl:variable>
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani &lt;luca.padovani@cs.unibo.it&gt;
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to &lt;luca.padovani@cs.unibo.it&gt;
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include &lt;gdome.h&gt;
<xsl:if test="$module = 'Events'">
#include &lt;gdome-events.h&gt;
#include "GdomeSmartDOMHelper.hh"
</xsl:if>

#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

<xsl:value-of select="@name"/>::<xsl:value-of select="@name"/>(Gdome<xsl:value-of select="@name"/>* obj)<xsl:choose>
  <xsl:when test="@inherits"> : <xsl:value-of select="@inherits"/>((Gdome<xsl:value-of select="@inherits"/>*) obj) { }

</xsl:when>
  <xsl:otherwise>
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_<xsl:value-of select="$prefix"/>_ref(gdome_obj, &amp;exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to <xsl:value-of select="@name"/> failed");
  }
}

</xsl:otherwise>
</xsl:choose>

<xsl:if test="@name = 'EventTarget'">
EventTarget::EventTarget(const Node&amp; obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &amp;exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to EventTarget failed");
  }
}

</xsl:if>

<xsl:if test="@name = 'Node'">
Node::Node(const EventTarget&amp; obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &amp;exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Node failed");
  }
}
</xsl:if>

<xsl:apply-templates select="." mode="cast">
  <xsl:with-param name="name" select="@name"/>
  <xsl:with-param name="inherits" select="@inherits"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>

<xsl:value-of select="@name"/>&amp; <xsl:value-of select="@name"/>::operator=(const <xsl:value-of select="@name"/>&amp; obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;<xsl:choose>
  <xsl:when test="@inherits">
    gdome_<xsl:value-of select="$prefix"/>_unref((Gdome<xsl:value-of select="@name"/>*) gdome_obj, &amp;exc_);
  </xsl:when>
  <xsl:otherwise>
    gdome_<xsl:value-of select="$prefix"/>_unref(gdome_obj, &amp;exc_);
  </xsl:otherwise>
</xsl:choose>  if (exc_ != 0) throw DOMException(exc_, "unref to <xsl:value-of select="@name"/> failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;<xsl:choose>
  <xsl:when test="@inherits">
    gdome_<xsl:value-of select="$prefix"/>_ref((Gdome<xsl:value-of select="@name"/>*) gdome_obj, &amp;exc_);
  </xsl:when>
  <xsl:otherwise>
    gdome_<xsl:value-of select="$prefix"/>_ref(gdome_obj, &amp;exc_);
  </xsl:otherwise>
</xsl:choose>  if (exc_ != 0) throw DOMException(exc_, "ref to <xsl:value-of select="@name"/> failed");
  }

  return *this;
}

<xsl:value-of select="@name"/>::~<xsl:value-of select="@name"/>()
{<xsl:if test="not(@inherits)">
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_<xsl:value-of select="$prefix"/>_unref(gdome_obj, &amp;exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to <xsl:value-of select="@name"/> failed");
    gdome_obj = 0;
  }
</xsl:if>
}

<xsl:if test="not(@inherits)">
Gdome<xsl:value-of select="@name"/>* <xsl:value-of select="@name"/>::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_<xsl:value-of select="$prefix"/>_ref(gdome_obj, &amp;exc_);
    if (exc_ != 0) throw DOMException(exc_, "<xsl:value-of select="@name"/>::gdome_object");
  }

  return gdome_obj;
}
</xsl:if>

<xsl:if test="@name = 'Node'">
extern "C" void* gdome_xml_n_get_xmlNode(GdomeNode*);
void* <xsl:value-of select="@name"/>::id() const
{
  if (gdome_obj != 0) return gdome_xml_n_get_xmlNode(gdome_obj);
  else return 0;
}
</xsl:if>

<!--
<xsl:if test="@name = 'Node'">
void*
Node::get_userData() const
{
  if (gdome_obj == 0) throw DOMException(GDOME_NULL_POINTER_ERR, "Node::get_userData");
  return gdome_obj-&gt;user_data;
}

void
Node::set_userData(void* v) const
{
  if (gdome_obj == 0) throw DOMException(GDOME_NULL_POINTER_ERR, "Node::set_userData");
  gdome_obj-&gt;user_data = v;
}

</xsl:if>
-->

<xsl:apply-templates select="attribute">
  <xsl:with-param name="interface" select="@name"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>

<xsl:apply-templates select="method">
  <xsl:with-param name="interface" select="@name"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>

}

</xsl:template>

<xsl:template match="interface" mode="cast">
  <xsl:param name="name" select="''"/>
  <xsl:param name="inherits" select="''"/>
  <xsl:param name="prefix" select="''"/>
  <xsl:value-of select="$name"/>::<xsl:value-of select="$name"/>(const <xsl:value-of select="@name"/>&amp; obj)<xsl:choose>
  <xsl:when test="$inherits">
    <xsl:choose>
      <xsl:when test="$name = @name"> : <xsl:value-of select="$inherits"/>(obj) { }

</xsl:when>
      <xsl:otherwise> : <xsl:value-of select="$inherits"/>((Gdome<xsl:value-of select="$inherits"/>*) gdome_cast_<xsl:value-of select="$prefix"/>(obj.gdome_obj)) { }

</xsl:otherwise>
    </xsl:choose>
  </xsl:when>
<xsl:otherwise>
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_<xsl:value-of select="$prefix"/>_ref(gdome_obj, &amp;exc_);
    if (exc_ != 0) throw DOMException(exc_, "<xsl:value-of select="$name"/>::<xsl:value-of select="$name"/> casting from <xsl:value-of select="@name"/>");
  }
}

</xsl:otherwise>
</xsl:choose>

<xsl:if test="@inherits">
<xsl:apply-templates select="document(concat($uriprefix, concat('/', concat(@inherits, '.xml'))))/interface" mode="cast">
  <xsl:with-param name="name" select="$name"/>
  <xsl:with-param name="inherits" select="$inherits"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>
  </xsl:if>
</xsl:template>

<xsl:template match="attribute">
  <xsl:param name="interface" select="''"/>
  <xsl:param name="prefix" select="''"/>
  <xsl:call-template name="returnTypeOfType">
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
  <xsl:value-of select="' '"/>
  <xsl:value-of select="$interface"/>::get_<xsl:value-of select="@name"/>() const<!-- throw(<xsl:apply-templates select="getraises/exception"/>)-->
{
  GdomeException exc_ = 0;
  <xsl:call-template name="gdome-result">
    <xsl:with-param name="type" select="@type"/>
    <xsl:with-param name="init">gdome_<xsl:value-of select="$prefix"/>_<xsl:value-of select="@name"/>((Gdome<xsl:value-of select="$interface"/>*) gdome_obj, &amp;exc_)</xsl:with-param>
  </xsl:call-template>;
  if (exc_ != 0) throw DOMException(exc_, "<xsl:value-of select="$interface"/>::get_<xsl:value-of select="@name"/>");
  <xsl:call-template name="return-result">
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
}

<xsl:if test="not(@readonly)">void
<xsl:value-of select="$interface"/>::set_<xsl:value-of select="@name"/>(const <xsl:call-template name="typeOfType"><xsl:with-param name="type" select="@type"/></xsl:call-template> v) const<!-- throw(<xsl:apply-templates select="setraises/exception"/>)-->
{
  GdomeException exc_ = 0;
  <xsl:call-template name="convert-param">
    <xsl:with-param name="name" select="'v'"/>
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>gdome_<xsl:value-of select="$prefix"/>_set_<xsl:value-of select="@name"/>((Gdome<xsl:value-of select="$interface"/>*) gdome_obj, <xsl:call-template name="pass-param"><xsl:with-param name="name" select="'v'"/><xsl:with-param name="type" select="@type"/></xsl:call-template>&amp;exc_);<xsl:call-template name="free-param">
    <xsl:with-param name="name" select="'v'"/>
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
  if (exc_ != 0) throw DOMException(exc_, "<xsl:value-of select="$interface"/>::set_<xsl:value-of select="@name"/>");
}

</xsl:if>
</xsl:template>

<xsl:template match="method" >
  <xsl:param name="interface" select="''"/>
  <xsl:param name="prefix" select="''"/>
  <xsl:call-template name="returnTypeOfType">
    <xsl:with-param name="type" select="returns/@type"/>
  </xsl:call-template>
  <xsl:value-of select="' '"/>
  <xsl:value-of select="$interface"/>::<xsl:value-of select="@name"/>(<xsl:apply-templates select="parameters"/>) const<!-- throw(<xsl:apply-templates select="raises/exception"/>)-->
{
  GdomeException exc_ = 0;
  <xsl:apply-templates select="parameters" mode="convert"/>
  <xsl:call-template name="gdome-result">
    <xsl:with-param name="type" select="returns/@type"/>
    <xsl:with-param name="init">gdome_<xsl:value-of select="$prefix"/>_<xsl:value-of select="@name"/>((Gdome<xsl:value-of select="$interface"/>*) gdome_obj, <xsl:apply-templates select="parameters" mode="pass"/>&amp;exc_)</xsl:with-param>
  </xsl:call-template>;
  <xsl:apply-templates select="parameters" mode="free"/>
  if (exc_ != 0) throw DOMException(exc_, "<xsl:value-of select="$interface"/>::<xsl:value-of select="@name"/>");
  <xsl:call-template name="return-result">
    <xsl:with-param name="type" select="returns/@type"/>
  </xsl:call-template>
}

</xsl:template>

<xsl:template match="parameters">
  <xsl:apply-templates select="param"/>
</xsl:template>

<xsl:template match="parameters" mode="convert">
  <xsl:apply-templates select="param" mode="convert"/>
</xsl:template>

<xsl:template match="parameters" mode="free">
  <xsl:apply-templates select="param" mode="free"/>
</xsl:template>

<xsl:template match="parameters" mode="pass">
  <xsl:apply-templates select="param" mode="pass"/>
</xsl:template>

<xsl:template match="parameters" mode="free-converted">
  <xsl:apply-templates select="param" mode="free-converted"/>
</xsl:template>

<xsl:template match="param">
  <xsl:if test="@attr = 'in'">const </xsl:if>
  <xsl:call-template name="typeOfType">
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
  <xsl:value-of select="' '"/>
  <xsl:value-of select="@name"/>
  <xsl:if test="position() &lt; last()">, </xsl:if>
</xsl:template>

<xsl:template match="param" mode="convert">
  <xsl:call-template name="convert-param">
    <xsl:with-param name="name" select="@name"/>
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="convert-param">
  <xsl:param name="name" select="''"/>
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <!--
    <xsl:when test="$type = 'DOMString'">GdomeDOMString* <xsl:value-of select="$name"/>_ = GdomeDOMString_of_DOMString(<xsl:value-of select="$name"/>);
  </xsl:when>-->
    <xsl:when test="$type = 'EventListener'">GdomeEventListener* <xsl:value-of select="$name"/>_ = gdome_evntl_mkref(my_gdome_event_listener_callback, const_cast&lt;void*&gt;(reinterpret_cast&lt;const void*&gt;(&amp;<xsl:value-of select="$name"/>)));
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="param" mode="pass">
  <xsl:call-template name="pass-param">
    <xsl:with-param name="name" select="@name"/>
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="pass-param">
  <xsl:param name="name" select="''"/>
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'boolean' or $type = 'unsigned short' or $type = 'unsigned long'"><xsl:value-of select="$name"/></xsl:when>
    <xsl:when test="$type = 'EventListener'"><xsl:value-of select="$name"/>_</xsl:when>
    <xsl:when test="$type = 'DOMString'">static_cast&lt;GdomeDOMString*&gt;(<xsl:value-of select="$name"/>)</xsl:when>
    <!--<xsl:when test="$type = 'DOMString' or $type = 'EventListener'"><xsl:value-of select="$name"/>_</xsl:when>-->
    <xsl:otherwise>(Gdome<xsl:value-of select="$type"/>*) <xsl:value-of select="$name"/>.gdome_obj</xsl:otherwise>
  </xsl:choose>, </xsl:template>

<xsl:template match="param" mode="free">
  <xsl:call-template name="free-param">
    <xsl:with-param name="name" select="@name"/>
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="free-param">
  <xsl:param name="name" select="''"/>
  <xsl:param name="type" select="''"/>
  <!--
  <xsl:if test="$type = 'DOMString'">
  if (<xsl:value-of select="$name"/>_ != 0) gdome_str_unref(<xsl:value-of select="$name"/>_);
  </xsl:if>
  -->
</xsl:template>

<xsl:template name="returnTypeOfType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'boolean'">bool</xsl:when>
    <xsl:when test="$type = 'DOMString'">GdomeString</xsl:when>
    <xsl:otherwise><xsl:value-of select="$type"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="typeOfType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'boolean'">bool</xsl:when>
    <xsl:when test="$type = 'unsigned short'">unsigned short</xsl:when>
    <xsl:when test="$type = 'unsigned long'">unsigned long</xsl:when>
    <xsl:when test="$type = 'DOMTimeStamp'">DOMTimeStamp</xsl:when>
    <xsl:when test="$type = 'DOMString'">GdomeString&amp;</xsl:when>
    <xsl:otherwise><xsl:value-of select="$type"/>&amp;</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="gdomeNodeTypeOfType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'DocumentFragment'">DOCUMENT_FRAGMENT</xsl:when>
    <xsl:when test="$type = 'Document'">DOCUMENT</xsl:when>
    <xsl:when test="$type = 'Attr'">ATTRIBUTE</xsl:when>
    <xsl:when test="$type = 'Element'">ELEMENT</xsl:when>
    <xsl:when test="$type = 'Text'">TEXT</xsl:when>
    <xsl:when test="$type = 'Comment'">COMMENT</xsl:when>
    <xsl:when test="$type = 'CDATASection'">CDATA_SECTION</xsl:when>
    <xsl:when test="$type = 'DocumentType'">DOCUMENT_TYPE</xsl:when>
    <xsl:when test="$type = 'Notation'">NOTATION</xsl:when>
    <xsl:when test="$type = 'Entity'">ENTITY</xsl:when>
    <xsl:when test="$type = 'EntityReference'">ENTITY_REFERENCE</xsl:when>
    <xsl:when test="$type = 'ProcessingInstruction'">PROCESSING_INSTRUCTION</xsl:when>
    <xsl:otherwise>type</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!--
<xsl:template name="gdome-result">
  <xsl:param name="type" select="''"/>
  <xsl:call-template name="gdomeTypeOfType">
    <xsl:with-param name="type" select="$type"/>
  </xsl:call-template>
  <xsl:choose>
    <xsl:when test="$type = 'void'"/>
    <xsl:when test="$type = 'boolean'"> res_</xsl:when>
    <xsl:when test="$type = 'unsigned short'"> res_</xsl:when>
    <xsl:when test="$type = 'unsigned long'"> res_</xsl:when>
    <xsl:when test="$type = 'DOMTimeStamp'"> res_</xsl:when>
    <xsl:otherwise> res__</xsl:otherwise>
  </xsl:choose>
</xsl:template>
-->
<xsl:template name="gdome-result">
  <xsl:param name="type" select="''"/>
  <xsl:param name="init" select="''"/>
  <xsl:if test="not($type = 'void')">
    <xsl:call-template name="returnTypeOfType">
      <xsl:with-param name="type" select="$type"/>
    </xsl:call-template>
  </xsl:if>
  <xsl:choose>
    <xsl:when test="$type = 'void'">
      <xsl:value-of select="$init"/>
    </xsl:when>
    <xsl:when test="$type = 'boolean' or $type = 'unsigned short' or $type = 'unsigned long' or $type = 'DOMTimeStamp'"> res_ = <xsl:value-of select="$init"/></xsl:when>
    <xsl:otherwise> res_(<xsl:value-of select="$init"/>, true)</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="return-result">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'void'"/>
    <xsl:otherwise>return res_;</xsl:otherwise>
    <!--
    <xsl:when test="$type = 'boolean'">return res_;</xsl:when>
    <xsl:when test="$type = 'unsigned short'">return res_;</xsl:when>
    <xsl:when test="$type = 'unsigned long'">return res_;</xsl:when>
    <xsl:when test="$type = 'DOMTimeStamp'">return res_;</xsl:when>
    <xsl:when test="$type = 'DOMString'">GdomeString res_(res__);
  if (res__ != 0) gdome_str_unref(res__);
  return res_;</xsl:when>
    <xsl:otherwise><xsl:value-of select="$type"/> res_(res__);
  if (res__ != 0) gdome_<xsl:call-template name="gdomePrefixOfType">
    <xsl:with-param name="type" select="$type"/>
  </xsl:call-template>_unref(res__, &amp;exc_);
  if (exc_ != 0) throw DOMException(exc_, "unref to <xsl:value-of select="$type"/> failed");
  return res_;</xsl:otherwise>
  -->
  </xsl:choose>
</xsl:template>

<xsl:template match="exception">
  <xsl:value-of select="@name"/>
  <xsl:if test="position() &lt; last()">, </xsl:if>
</xsl:template>

</xsl:stylesheet>

