/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <config.h>

#include <gdome.h>
#include <gdome-util.h>

#include "GdomeSmartDOM.hh"

namespace GdomeSmartDOM {

DOMImplementation::DOMImplementation()
{
  gdome_obj = gdome_di_mkref();
}

DOMImplementation::DOMImplementation(const DOMImplementation& obj)
{
  gdome_obj = obj.gdome_obj;
  GdomeException exc_ = 0;
  gdome_di_ref(gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::DOMImplementation");
}

DOMImplementation::DOMImplementation(GdomeDOMImplementation* obj)
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_di_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::DOMImplementation");
  }
}

DOMImplementation::~DOMImplementation()
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_di_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::~DOMImplementation");
    gdome_obj = 0;
  }
}

bool
DOMImplementation::hasFeature(const GdomeString& feature, const GdomeString& version)
{
  GdomeException exc_ = 0;
  GdomeBoolean res_ = gdome_di_hasFeature(gdome_obj,
					  feature.gdome_str(),
					  version.gdome_str(), &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::hasFeature");
  return res_;
}

DocumentType
DOMImplementation::createDocumentType(const GdomeString& qualifiedName,
				      const GdomeString& publicId,
				      const GdomeString& systemId)
{
  GdomeException exc_ = 0;
  GdomeDocumentType* res__ = gdome_di_createDocumentType(gdome_obj,
							qualifiedName.gdome_str(),
							publicId.gdome_str(),
							systemId.gdome_str(), &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::createDocumentType");
  DocumentType res_(res__);
  gdome_dt_unref(res__, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "unref to DocumentType failed");
  return res_;
}

Document
DOMImplementation::createDocument(const GdomeString& namespaceURI,
				  const GdomeString& qualifiedName,
				  const DocumentType& doctype)
{
  GdomeException exc_ = 0;
  GdomeDocument* res__ = gdome_di_createDocument(gdome_obj,
						namespaceURI.gdome_str(),
						qualifiedName.gdome_str(),
						(GdomeDocumentType*) doctype.gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::createDocument");
  Document res_(res__);
  gdome_doc_unref(res__, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "unref to Document failed");
  return res_;
}

Document
DOMImplementation::createDocumentFromURI(const char* uri, unsigned long mode)
{
  GdomeException exc_ = 0;
  GdomeDocument* res__ = gdome_di_createDocFromURI(gdome_obj, uri, mode, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::createDocumentFromURI");
  Document res_(res__);
  gdome_doc_unref(res__, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "unref to Document failed");
  return res_;
}

Document
DOMImplementation::createDocumentFromMemory(const char* buffer, unsigned long mode)
{
  GdomeException exc_ = 0;
  GdomeDocument* res__ = gdome_di_createDocFromMemory(gdome_obj, const_cast<char*>(buffer), mode, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::createDocumentFromMemory");
  Document res_(res__);
  gdome_doc_unref(res__, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "unref to Document failed");
  return res_;
}

bool
DOMImplementation::saveDocumentToFile(const Document& doc, const char* fileName, unsigned long mode)
{
  GdomeException exc_ = 0;
  GdomeBoolean res_ = gdome_di_saveDocToFile(gdome_obj,
					     (GdomeDocument*) doc.gdome_obj,
					     fileName, (GdomeSavingCode) mode, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::saveDocumentToFile");
  return res_;
}

bool
DOMImplementation::saveDocumentToMemory(const Document& doc, GdomeString& res, unsigned long mode)
{
  GdomeException exc_ = 0;
  char* mem = 0;
  GdomeBoolean res_ = gdome_di_saveDocToMemory(gdome_obj,
					       (GdomeDocument*) doc.gdome_obj,
					       &mem, (GdomeSavingCode) mode, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::saveDocumentToMemory");
  if (res_) res = mem;
  return res_;
}

void
DOMImplementation::enableEvent(const Document& doc, const char* name)
{
  GdomeException exc_ = 0;
  gdome_di_enableEvent(gdome_obj, (GdomeDocument*) doc.gdome_obj, name, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::enableEvent");
}

void
DOMImplementation::disableEvent(const Document& doc, const char* name)
{
  GdomeException exc_ = 0;
  gdome_di_disableEvent(gdome_obj, (GdomeDocument*) doc.gdome_obj, name, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::disableEvent");
}

bool
DOMImplementation::eventIsEnabled(const Document& doc, const char* name)
{
  GdomeException exc_ = 0;
  GdomeBoolean res_ = gdome_di_eventIsEnabled(gdome_obj, (GdomeDocument*) doc.gdome_obj, name, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::eventIsEnabled");
  return res_;
}

GdomeDOMImplementation* DOMImplementation::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_di_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "DOMImplementation::gdome_object");
  }

  return gdome_obj;
}

}

