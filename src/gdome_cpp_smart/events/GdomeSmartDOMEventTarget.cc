
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>

#include <gdome-events.h>
#include "GdomeSmartDOMHelper.hh"


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

EventTarget::EventTarget(GdomeEventTarget* obj)
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to EventTarget failed");
  }
}


EventTarget::EventTarget(const Node& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to EventTarget failed");
  }
}

EventTarget::EventTarget(const EventTarget& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "EventTarget::EventTarget casting from EventTarget");
  }
}

EventTarget& EventTarget::operator=(const EventTarget& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to EventTarget failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to EventTarget failed");
  }

  return *this;
}

EventTarget::~EventTarget()
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to EventTarget failed");
    gdome_obj = 0;
  }

}


GdomeEventTarget* EventTarget::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_n_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "EventTarget::gdome_object");
  }

  return gdome_obj;
}
void EventTarget::addEventListener(const GdomeString& type, const EventListener& listener, const bool useCapture) const
{
  GdomeException exc_ = 0;
  GdomeEventListener* listener_ = gdome_evntl_mkref(my_gdome_event_listener_callback, const_cast<void*>(reinterpret_cast<const void*>(&listener)));
    gdome_n_addEventListener((GdomeEventTarget*) gdome_obj, static_cast<GdomeDOMString*>(type), listener_, useCapture, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "EventTarget::addEventListener");
  
}

void EventTarget::removeEventListener(const GdomeString& type, const EventListener& listener, const bool useCapture) const
{
  GdomeException exc_ = 0;
  GdomeEventListener* listener_ = gdome_evntl_mkref(my_gdome_event_listener_callback, const_cast<void*>(reinterpret_cast<const void*>(&listener)));
    gdome_n_removeEventListener((GdomeEventTarget*) gdome_obj, static_cast<GdomeDOMString*>(type), listener_, useCapture, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "EventTarget::removeEventListener");
  
}

bool EventTarget::dispatchEvent(const Event& evt) const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_n_dispatchEvent((GdomeEventTarget*) gdome_obj, (GdomeEvent*) evt.gdome_obj, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "EventTarget::dispatchEvent");
  return res_;
}



}

