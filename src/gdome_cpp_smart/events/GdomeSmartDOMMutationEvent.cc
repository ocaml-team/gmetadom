
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>

#include <gdome-events.h>
#include "GdomeSmartDOMHelper.hh"


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

MutationEvent::MutationEvent(GdomeMutationEvent* obj) : Event((GdomeEvent*) obj) { }

MutationEvent::MutationEvent(const MutationEvent& obj) : Event(obj) { }

MutationEvent::MutationEvent(const Event& obj) : Event((GdomeEvent*) gdome_cast_mevnt(obj.gdome_obj)) { }

MutationEvent& MutationEvent::operator=(const MutationEvent& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_mevnt_unref((GdomeMutationEvent*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to MutationEvent failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_mevnt_ref((GdomeMutationEvent*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to MutationEvent failed");
  }

  return *this;
}

MutationEvent::~MutationEvent()
{
}

Node MutationEvent::get_relatedNode() const
{
  GdomeException exc_ = 0;
  Node res_(gdome_mevnt_relatedNode((GdomeMutationEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "MutationEvent::get_relatedNode");
  return res_;
}

GdomeString MutationEvent::get_prevValue() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_mevnt_prevValue((GdomeMutationEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "MutationEvent::get_prevValue");
  return res_;
}

GdomeString MutationEvent::get_newValue() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_mevnt_newValue((GdomeMutationEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "MutationEvent::get_newValue");
  return res_;
}

GdomeString MutationEvent::get_attrName() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_mevnt_attrName((GdomeMutationEvent*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "MutationEvent::get_attrName");
  return res_;
}

unsigned short MutationEvent::get_attrChange() const
{
  GdomeException exc_ = 0;
  unsigned short res_ = gdome_mevnt_attrChange((GdomeMutationEvent*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "MutationEvent::get_attrChange");
  return res_;
}

void MutationEvent::initMutationEvent(const GdomeString& typeArg, const bool canBubbleArg, const bool cancelableArg, const Node& relatedNodeArg, const GdomeString& prevValueArg, const GdomeString& newValueArg, const GdomeString& attrNameArg, const unsigned short attrChangeArg) const
{
  GdomeException exc_ = 0;
  gdome_mevnt_initMutationEvent((GdomeMutationEvent*) gdome_obj, static_cast<GdomeDOMString*>(typeArg), canBubbleArg, cancelableArg, (GdomeNode*) relatedNodeArg.gdome_obj, static_cast<GdomeDOMString*>(prevValueArg), static_cast<GdomeDOMString*>(newValueArg), static_cast<GdomeDOMString*>(attrNameArg), attrChangeArg, &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "MutationEvent::initMutationEvent");
  
}



}

