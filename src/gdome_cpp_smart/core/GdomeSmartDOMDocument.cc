
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Document::Document(GdomeDocument* obj) : Node((GdomeNode*) obj) { }

Document::Document(const Document& obj) : Node(obj) { }

Document::Document(const Node& obj) : Node((GdomeNode*) gdome_cast_doc(obj.gdome_obj)) { }

Document& Document::operator=(const Document& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_doc_unref((GdomeDocument*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Document failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_doc_ref((GdomeDocument*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Document failed");
  }

  return *this;
}

Document::~Document()
{
}

DocumentType Document::get_doctype() const
{
  GdomeException exc_ = 0;
  DocumentType res_(gdome_doc_doctype((GdomeDocument*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Document::get_doctype");
  return res_;
}

DOMImplementation Document::get_implementation() const
{
  GdomeException exc_ = 0;
  DOMImplementation res_(gdome_doc_implementation((GdomeDocument*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Document::get_implementation");
  return res_;
}

Element Document::get_documentElement() const
{
  GdomeException exc_ = 0;
  Element res_(gdome_doc_documentElement((GdomeDocument*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Document::get_documentElement");
  return res_;
}

Element Document::createElement(const GdomeString& tagName) const
{
  GdomeException exc_ = 0;
  Element res_(gdome_doc_createElement((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(tagName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createElement");
  return res_;
}

DocumentFragment Document::createDocumentFragment() const
{
  GdomeException exc_ = 0;
  DocumentFragment res_(gdome_doc_createDocumentFragment((GdomeDocument*) gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createDocumentFragment");
  return res_;
}

Text Document::createTextNode(const GdomeString& data) const
{
  GdomeException exc_ = 0;
  Text res_(gdome_doc_createTextNode((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(data), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createTextNode");
  return res_;
}

Comment Document::createComment(const GdomeString& data) const
{
  GdomeException exc_ = 0;
  Comment res_(gdome_doc_createComment((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(data), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createComment");
  return res_;
}

CDATASection Document::createCDATASection(const GdomeString& data) const
{
  GdomeException exc_ = 0;
  CDATASection res_(gdome_doc_createCDATASection((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(data), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createCDATASection");
  return res_;
}

ProcessingInstruction Document::createProcessingInstruction(const GdomeString& target, const GdomeString& data) const
{
  GdomeException exc_ = 0;
  ProcessingInstruction res_(gdome_doc_createProcessingInstruction((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(target), static_cast<GdomeDOMString*>(data), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createProcessingInstruction");
  return res_;
}

Attr Document::createAttribute(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_doc_createAttribute((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createAttribute");
  return res_;
}

EntityReference Document::createEntityReference(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  EntityReference res_(gdome_doc_createEntityReference((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createEntityReference");
  return res_;
}

NodeList Document::getElementsByTagName(const GdomeString& tagname) const
{
  GdomeException exc_ = 0;
  NodeList res_(gdome_doc_getElementsByTagName((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(tagname), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::getElementsByTagName");
  return res_;
}

Node Document::importNode(const Node& importedNode, const bool deep) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_doc_importNode((GdomeDocument*) gdome_obj, (GdomeNode*) importedNode.gdome_obj, deep, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::importNode");
  return res_;
}

Element Document::createElementNS(const GdomeString& namespaceURI, const GdomeString& qualifiedName) const
{
  GdomeException exc_ = 0;
  Element res_(gdome_doc_createElementNS((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(qualifiedName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createElementNS");
  return res_;
}

Attr Document::createAttributeNS(const GdomeString& namespaceURI, const GdomeString& qualifiedName) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_doc_createAttributeNS((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(qualifiedName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::createAttributeNS");
  return res_;
}

NodeList Document::getElementsByTagNameNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  NodeList res_(gdome_doc_getElementsByTagNameNS((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::getElementsByTagNameNS");
  return res_;
}

Element Document::getElementById(const GdomeString& elementId) const
{
  GdomeException exc_ = 0;
  Element res_(gdome_doc_getElementById((GdomeDocument*) gdome_obj, static_cast<GdomeDOMString*>(elementId), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Document::getElementById");
  return res_;
}



}

