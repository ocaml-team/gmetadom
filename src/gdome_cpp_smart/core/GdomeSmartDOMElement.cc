
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Element::Element(GdomeElement* obj) : Node((GdomeNode*) obj) { }

Element::Element(const Element& obj) : Node(obj) { }

Element::Element(const Node& obj) : Node((GdomeNode*) gdome_cast_el(obj.gdome_obj)) { }

Element& Element::operator=(const Element& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_el_unref((GdomeElement*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Element failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_el_ref((GdomeElement*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Element failed");
  }

  return *this;
}

Element::~Element()
{
}

GdomeString Element::get_tagName() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_el_tagName((GdomeElement*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Element::get_tagName");
  return res_;
}

GdomeString Element::getAttribute(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_el_getAttribute((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::getAttribute");
  return res_;
}

void Element::setAttribute(const GdomeString& name, const GdomeString& value) const
{
  GdomeException exc_ = 0;
  gdome_el_setAttribute((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(name), static_cast<GdomeDOMString*>(value), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::setAttribute");
  
}

void Element::removeAttribute(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  gdome_el_removeAttribute((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::removeAttribute");
  
}

Attr Element::getAttributeNode(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_el_getAttributeNode((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::getAttributeNode");
  return res_;
}

Attr Element::setAttributeNode(const Attr& newAttr) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_el_setAttributeNode((GdomeElement*) gdome_obj, (GdomeAttr*) newAttr.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::setAttributeNode");
  return res_;
}

Attr Element::removeAttributeNode(const Attr& oldAttr) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_el_removeAttributeNode((GdomeElement*) gdome_obj, (GdomeAttr*) oldAttr.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::removeAttributeNode");
  return res_;
}

NodeList Element::getElementsByTagName(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  NodeList res_(gdome_el_getElementsByTagName((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::getElementsByTagName");
  return res_;
}

GdomeString Element::getAttributeNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_el_getAttributeNS((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::getAttributeNS");
  return res_;
}

void Element::setAttributeNS(const GdomeString& namespaceURI, const GdomeString& qualifiedName, const GdomeString& value) const
{
  GdomeException exc_ = 0;
  gdome_el_setAttributeNS((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(qualifiedName), static_cast<GdomeDOMString*>(value), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::setAttributeNS");
  
}

void Element::removeAttributeNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  gdome_el_removeAttributeNS((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::removeAttributeNS");
  
}

Attr Element::getAttributeNodeNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_el_getAttributeNodeNS((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::getAttributeNodeNS");
  return res_;
}

Attr Element::setAttributeNodeNS(const Attr& newAttr) const
{
  GdomeException exc_ = 0;
  Attr res_(gdome_el_setAttributeNodeNS((GdomeElement*) gdome_obj, (GdomeAttr*) newAttr.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::setAttributeNodeNS");
  return res_;
}

NodeList Element::getElementsByTagNameNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  NodeList res_(gdome_el_getElementsByTagNameNS((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::getElementsByTagNameNS");
  return res_;
}

bool Element::hasAttribute(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_el_hasAttribute((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::hasAttribute");
  return res_;
}

bool Element::hasAttributeNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  bool res_ = gdome_el_hasAttributeNS((GdomeElement*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_);
  
  if (exc_ != 0) throw DOMException(exc_, "Element::hasAttributeNS");
  return res_;
}



}

