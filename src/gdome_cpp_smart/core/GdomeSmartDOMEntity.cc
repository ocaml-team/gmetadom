
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

Entity::Entity(GdomeEntity* obj) : Node((GdomeNode*) obj) { }

Entity::Entity(const Entity& obj) : Node(obj) { }

Entity::Entity(const Node& obj) : Node((GdomeNode*) gdome_cast_ent(obj.gdome_obj)) { }

Entity& Entity::operator=(const Entity& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_ent_unref((GdomeEntity*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to Entity failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_ent_ref((GdomeEntity*) gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to Entity failed");
  }

  return *this;
}

Entity::~Entity()
{
}

GdomeString Entity::get_publicId() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_ent_publicId((GdomeEntity*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Entity::get_publicId");
  return res_;
}

GdomeString Entity::get_systemId() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_ent_systemId((GdomeEntity*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Entity::get_systemId");
  return res_;
}

GdomeString Entity::get_notationName() const
{
  GdomeException exc_ = 0;
  GdomeString res_(gdome_ent_notationName((GdomeEntity*) gdome_obj, &exc_), true);
  if (exc_ != 0) throw DOMException(exc_, "Entity::get_notationName");
  return res_;
}



}

