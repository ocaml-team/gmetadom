
/* This file is part of MetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001  Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the author's home page
 * http://www.cs.unibo.it/~lpadovan
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by MetaDOM
 */

#include <gdome.h>


#include "GdomeSmartDOMBasic.hh"
#include "GdomeSmartDOMDOMImplementation.hh"
#include "GdomeSmartDOMCore.hh"
#include "GdomeSmartDOMEvents.hh"

namespace GdomeSmartDOM {

NamedNodeMap::NamedNodeMap(GdomeNamedNodeMap* obj)
{
  gdome_obj = obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nnm_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to NamedNodeMap failed");
  }
}

NamedNodeMap::NamedNodeMap(const NamedNodeMap& obj)
{
  gdome_obj = obj.gdome_obj;
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nnm_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::NamedNodeMap casting from NamedNodeMap");
  }
}

NamedNodeMap& NamedNodeMap::operator=(const NamedNodeMap& obj)
{
  if (gdome_obj == obj.gdome_obj) return *this;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nnm_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to NamedNodeMap failed");
  }

  gdome_obj = obj.gdome_obj;

  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nnm_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "ref to NamedNodeMap failed");
  }

  return *this;
}

NamedNodeMap::~NamedNodeMap()
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nnm_unref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "unref to NamedNodeMap failed");
    gdome_obj = 0;
  }

}


GdomeNamedNodeMap* NamedNodeMap::gdome_object() const
{
  if (gdome_obj != 0) {
    GdomeException exc_ = 0;
    gdome_nnm_ref(gdome_obj, &exc_);
    if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::gdome_object");
  }

  return gdome_obj;
}
unsigned long NamedNodeMap::get_length() const
{
  GdomeException exc_ = 0;
  unsigned long res_ = gdome_nnm_length((GdomeNamedNodeMap*) gdome_obj, &exc_);
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::get_length");
  return res_;
}

Node NamedNodeMap::getNamedItem(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_getNamedItem((GdomeNamedNodeMap*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::getNamedItem");
  return res_;
}

Node NamedNodeMap::setNamedItem(const Node& arg) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_setNamedItem((GdomeNamedNodeMap*) gdome_obj, (GdomeNode*) arg.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::setNamedItem");
  return res_;
}

Node NamedNodeMap::removeNamedItem(const GdomeString& name) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_removeNamedItem((GdomeNamedNodeMap*) gdome_obj, static_cast<GdomeDOMString*>(name), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::removeNamedItem");
  return res_;
}

Node NamedNodeMap::item(const unsigned long index) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_item((GdomeNamedNodeMap*) gdome_obj, index, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::item");
  return res_;
}

Node NamedNodeMap::getNamedItemNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_getNamedItemNS((GdomeNamedNodeMap*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::getNamedItemNS");
  return res_;
}

Node NamedNodeMap::setNamedItemNS(const Node& arg) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_setNamedItemNS((GdomeNamedNodeMap*) gdome_obj, (GdomeNode*) arg.gdome_obj, &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::setNamedItemNS");
  return res_;
}

Node NamedNodeMap::removeNamedItemNS(const GdomeString& namespaceURI, const GdomeString& localName) const
{
  GdomeException exc_ = 0;
  Node res_(gdome_nnm_removeNamedItemNS((GdomeNamedNodeMap*) gdome_obj, static_cast<GdomeDOMString*>(namespaceURI), static_cast<GdomeDOMString*>(localName), &exc_), true);
  
  if (exc_ != 0) throw DOMException(exc_, "NamedNodeMap::removeNamedItemNS");
  return res_;
}



}

