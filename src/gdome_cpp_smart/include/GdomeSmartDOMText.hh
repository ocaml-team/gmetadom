
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMText_hh__
#define __GdomeSmartDOMText_hh__

#include "GdomeSmartDOMCharacterData.hh"

namespace GdomeSmartDOM {

class Text : public CharacterData
{
protected:
  explicit Text(GdomeText* obj, bool) : CharacterData((GdomeCharacterData*) obj, true) { }
public:
  explicit Text(GdomeText* = 0);
    Text(const Text&);
  Text(const CharacterData&);
  Text(const Node&);
  ~Text();
Text& operator=(const Text&);
  bool operator==(const Text& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const Text& obj) const { return !(*this == obj); }
  
  // Attributes  
  
  // Methods
  class Text splitText(const unsigned long offset) const;
  
  // Friend classes
  friend class Document;
  };

}

#endif // __GdomeSmartDOMText_hh__

