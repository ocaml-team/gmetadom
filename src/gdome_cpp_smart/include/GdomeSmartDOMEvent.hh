
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMEvent_hh__
#define __GdomeSmartDOMEvent_hh__

namespace GdomeSmartDOM {

class Event
{
protected:
  explicit Event(GdomeEvent* obj, bool) : gdome_obj(obj) { }
public:
  explicit Event(GdomeEvent* = 0);
    Event(const Event&);
  ~Event();
Event& operator=(const Event&);
  bool operator==(const Event& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const Event& obj) const { return !(*this == obj); }
  
  operator GdomeEvent*() const { return reinterpret_cast<GdomeEvent*>(gdome_obj); }
  enum PhaseType {
    CAPTURING_PHASE = 1,
    AT_TARGET = 2,
    BUBBLING_PHASE = 3
  };

  // Attributes  
  class GdomeString get_type(void) const;
  class EventTarget get_target(void) const;
  class EventTarget get_currentTarget(void) const;
  unsigned short get_eventPhase(void) const;
  bool get_bubbles(void) const;
  bool get_cancelable(void) const;
  DOMTimeStamp get_timeStamp(void) const;
  
  // Methods
  void stopPropagation(void) const;
  void preventDefault(void) const;
  void initEvent(const class GdomeString& eventTypeArg, const bool canBubbleArg, const bool cancelableArg) const;
  
  // Friend classes
  friend class Document;
  
  friend class EventTarget;
  friend class MutationEvent;
      GdomeEvent* gdome_object(void) const;
  
protected:
  GdomeEvent* gdome_obj;
};

}

#endif // __GdomeSmartDOMEvent_hh__

