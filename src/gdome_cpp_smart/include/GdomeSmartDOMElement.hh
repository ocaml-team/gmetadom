
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#ifndef __GdomeSmartDOMElement_hh__
#define __GdomeSmartDOMElement_hh__

#include "GdomeSmartDOMNode.hh"

namespace GdomeSmartDOM {

class Element : public Node
{
protected:
  explicit Element(GdomeElement* obj, bool) : Node((GdomeNode*) obj, true) { }
public:
  explicit Element(GdomeElement* = 0);
    Element(const Element&);
  Element(const Node&);
  ~Element();
Element& operator=(const Element&);
  bool operator==(const Element& obj) const { return gdome_obj == obj.gdome_obj; }
  bool operator!=(const Element& obj) const { return !(*this == obj); }
  
  // Attributes  
  class GdomeString get_tagName(void) const;
  
  // Methods
  class GdomeString getAttribute(const class GdomeString& name) const;
  void setAttribute(const class GdomeString& name, const class GdomeString& value) const;
  void removeAttribute(const class GdomeString& name) const;
  class Attr getAttributeNode(const class GdomeString& name) const;
  class Attr setAttributeNode(const class Attr& newAttr) const;
  class Attr removeAttributeNode(const class Attr& oldAttr) const;
  class NodeList getElementsByTagName(const class GdomeString& name) const;
  class GdomeString getAttributeNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  void setAttributeNS(const class GdomeString& namespaceURI, const class GdomeString& qualifiedName, const class GdomeString& value) const;
  void removeAttributeNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  class Attr getAttributeNodeNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  class Attr setAttributeNodeNS(const class Attr& newAttr) const;
  class NodeList getElementsByTagNameNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  bool hasAttribute(const class GdomeString& name) const;
  bool hasAttributeNS(const class GdomeString& namespaceURI, const class GdomeString& localName) const;
  
  // Friend classes
  friend class Document;
  friend class Attr;
};

}

#endif // __GdomeSmartDOMElement_hh__

