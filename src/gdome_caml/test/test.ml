open Gdome;;

let di = domImplementation ()

let doc = di#createDocumentFromURI ~uri:(Sys.argv.(1)) ()
(* let _ = di#disableEvent doc "*" *)

let root = doc#get_documentElement
let root' = doc#get_documentElement
let _ = Printf.printf "comparing roots: %s\n" (if root#isSameNode (root' :> Gdome.node) then "OK" else "NO!")
let _ = Printf.printf "comparing roots ===: %s\n" (if (root :> Gdome.node) === (root' :> Gdome.node) then "OK" else "NO!")

let s1 = domString "hello"
let s2 = domString "hello"
let _ = Printf.printf "comparing dom strings: %s\n" (if s1 === s2 then "OK" else "NO!") 

let old = (root#getAttribute (domString "hello-spank"))

let listener = eventListener
 (function e ->
   Printf.printf "Event type: %s\n" (e#get_type#to_string) ;
   let me = new mutationEvent_of_event e in
   begin
    match me#get_relatedNode with
       None -> Printf.printf "no related node\n"
     | Some n -> Printf.printf "related node: %s\n" n#get_nodeName#to_string
   end ;
   begin
    match me#get_attrName, me#get_prevValue, me#get_newValue with
       Some attrName, Some prevValue, Some newValue -> Printf.printf "attrName = %s prev = %s new = %s\n" attrName#to_string prevValue#to_string newValue#to_string 
     | _ -> ()
   end ;
   Printf.printf "hello " ; flush stdout)

let _ = root#addEventListener (domString "DOMAttrModified") listener false

let waste n =
 let rec aux n s =
  if Random.int 1000 = 0 then root#setAttribute (domString "value") (domString (string_of_int n)) ;
  if n = 0 then s else aux (n-1) ("hello world!" :: s)
 in
  aux n []

let _ = Printf.printf "test 1\n" ; flush stdout

let _ = waste 5000000

let _ = Printf.printf "test 2\n" ; flush stdout

let _ =
 root#setAttribute (domString "hello-spank")
  (domString (old#to_string ^ " and Aika"))
;;

let _ = Printf.printf "test 3\n" ; flush stdout

let _ = waste 5000000

let _ = Printf.printf "test 4\n" ; flush stdout

let _ =
 root#setAttribute (domString "hello-spank")
  (domString (old#to_string ^ " and Luca"))
;;

let res_string = di#saveDocumentToMemory ~doc ~indent:true ();;

print_endline ("Result saveDocumentToMemory: **" ^ res_string ^ "**");;

let res = di#saveDocumentToFile ~doc ~name:"hi.xml" ~indent:true ()

let _ = Printf.printf "done!\n"
