<?xml version="1.0"?>

<!--
 This file is part of GMetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
               2001 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the project home page
 http://gmetadom.sourceforge.net
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="xslutils.xsl"/>

<xsl:output method="text"/>
<xsl:param name="module" select="'Core'"/>
<xsl:param name="uriprefix" select="''"/>
<xsl:param name="annotations" select="''"/>

<xsl:template match="interface">
<xsl:variable name="prefix">
  <xsl:call-template name="gdomePrefixOfType">
    <xsl:with-param name="type" select="@name"/>
  </xsl:call-template>
</xsl:variable>
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani &lt;luca.padovani@cs.unibo.it&gt;
 *               2002 Claudio Sacerdoti Coen &lt;sacerdot@cs.unibo.it&gt;
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to &lt;luca.padovani@cs.unibo.it&gt;
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)

<xsl:apply-templates select="." mode="cast">
  <xsl:with-param name="name" select="@name"/>
  <xsl:with-param name="inherits" select="@inherits"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>

<xsl:apply-templates select="attribute">
  <xsl:with-param name="interface" select="@name"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>

<xsl:if test="@name = 'Node'">
external isSameNode : this:[&gt; `<xsl:value-of select="@name"/>] GdomeT.t -&gt; [&gt; `Node] GdomeT.t -&gt; bool = "ml_gdome_<xsl:value-of select="$prefix"/>_isSameNode"
</xsl:if>

<xsl:apply-templates select="method">
  <xsl:with-param name="interface" select="@name"/>
  <xsl:with-param name="prefix" select="$prefix"/>
</xsl:apply-templates>

</xsl:template>

<xsl:template match="interface" mode="cast">
  <xsl:param name="name" select="''"/>
  <xsl:param name="prefix" select="''"/>
  
  <xsl:if test="@inherits">
   <xsl:variable name="parent" select="document(concat($uriprefix, concat('/', concat(@inherits, '.xml'))))/interface"/>
   <xsl:choose>
    <xsl:when test="$parent/@inherits">
     <xsl:apply-templates select="$parent" mode="cast">
      <xsl:with-param name="name" select="$name"/>
      <xsl:with-param name="prefix" select="$prefix"/>
     </xsl:apply-templates>
    </xsl:when>
    <xsl:otherwise>
external of_<xsl:value-of select="@inherits"/> : [&gt; `<xsl:value-of select="@inherits"/>] GdomeT.t -&gt; T<xsl:value-of select="$name"/>.t = "ml_gdome_<xsl:value-of select="$prefix"/>_of_<xsl:call-template name="gdomePrefixOfType"><xsl:with-param name="type"><xsl:value-of select="@inherits"/></xsl:with-param></xsl:call-template>"

  </xsl:otherwise>
  </xsl:choose>
 </xsl:if>
</xsl:template>

<xsl:template match="attribute">
  <xsl:param name="interface" select="''"/>
  <xsl:param name="prefix" select="''"/>
external get_<xsl:value-of select="@name"/> : this:[&gt; `<xsl:value-of select="$interface"/>] GdomeT.t -&gt; <xsl:choose>
  <xsl:when test="@name = 'nodeType' and $interface = 'Node'">GdomeNodeTypeT.t</xsl:when>
  <xsl:otherwise>
    <xsl:call-template name="attrType">
      <xsl:with-param name="name" select="@name"/>
      <xsl:with-param name="type" select="@type"/>
      <xsl:with-param name="invariant" select="true()"/>
    </xsl:call-template>
  </xsl:otherwise>
  </xsl:choose> = "ml_gdome_<xsl:value-of select="$prefix"/>_get_<xsl:value-of select="@name"/>"

<xsl:if test="not(@readonly)">
external set_<xsl:value-of select="@name"/> : this:[&gt; `<xsl:value-of select="$interface"/>] GdomeT.t -&gt; value:<xsl:call-template name="attrType">
    <xsl:with-param name="name" select="@name"/>
    <xsl:with-param name="type" select="@type"/>
    <xsl:with-param name="invariant" select="false()"/>
  </xsl:call-template> -&gt; unit = "ml_gdome_<xsl:value-of select="$prefix"/>_set_<xsl:value-of select="@name"/>"
</xsl:if>
</xsl:template>

<xsl:template match="method" >
  <xsl:param name="interface" select="''"/>
  <xsl:param name="prefix" select="''"/>
external <xsl:value-of select="@name"/> : this:[&gt; `<xsl:value-of select="$interface"/>] GdomeT.t<xsl:apply-templates select="parameters">
    <xsl:with-param name="name" select="@name"/>
  </xsl:apply-templates> -&gt; <xsl:call-template name="methodType">
    <xsl:with-param name="name" select="@name"/>
    <xsl:with-param name="type" select="returns/@type"/>
  </xsl:call-template> = "ml_gdome_<xsl:value-of select="$prefix"/>_<xsl:value-of select="@name"/>"<xsl:if test="count(parameters/param) &gt; 4"> "ml_gdome_<xsl:value-of select="$prefix"/>_<xsl:value-of select="@name"/>"</xsl:if>
</xsl:template>

<xsl:template match="parameters">
  <xsl:param name="name" select="''"/>
  <xsl:apply-templates select="param">
    <xsl:with-param name="methodName" select="$name"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="param">
  <xsl:param name="methodName" select="''"/> -&gt; <xsl:call-template name="ocamlify-param-name"><xsl:with-param name="name" select="@name"/></xsl:call-template>:<xsl:call-template name="paramType">
    <xsl:with-param name="methodName" select="$methodName"/>
    <xsl:with-param name="name" select="@name"/>
    <xsl:with-param name="type" select="@type"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="ocamlify-param-name">
  <xsl:param name="name" select="''"/>
  <xsl:choose>
    <xsl:when test="$name = 'type'">typ</xsl:when>
    <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="attrType">
  <xsl:param name="name" select="''"/>
  <xsl:param name="type" select="''"/>
  <xsl:param name="invariant" select="true()"/>
  <xsl:call-template name="typeOfType">
    <xsl:with-param name="type" select="$type"/>
    <xsl:with-param name="nullable" select="document($annotations)/Annotations/Attribute[@name = $name]/@nullable"/>
    <xsl:with-param name="invariant" select="$invariant"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="methodType">
  <xsl:param name="name" select="''"/>
  <xsl:param name="type" select="''"/>
  <xsl:call-template name="typeOfType">
    <xsl:with-param name="type" select="$type"/>
    <xsl:with-param name="nullable" select="document($annotations)/Annotations/Method[@name = $name]/@nullable"/>
    <xsl:with-param name="invariant" select="true()"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="paramType">
  <xsl:param name="methodName" select="''"/>
  <xsl:param name="name" select="''"/>
  <xsl:param name="type" select="''"/>
  <xsl:call-template name="typeOfType">
    <xsl:with-param name="type" select="$type"/>
    <xsl:with-param name="nullable" select="document($annotations)/Annotations/Method[@name = $methodName]/Param[@name = $name]/@nullable"/>
    <xsl:with-param name="invariant" select="false()"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="typeOfType">
  <xsl:param name="type" select="''"/>
  <xsl:param name="nullable" select="''"/>
  <xsl:param name="invariant" select="true()"/>
  <xsl:choose>
    <xsl:when test="$type = 'void'">unit</xsl:when>
    <xsl:when test="$type = 'boolean'">bool</xsl:when>
    <xsl:when test="$type = 'unsigned short'">int</xsl:when>
    <xsl:when test="$type = 'unsigned long'">int</xsl:when>
    <xsl:when test="$type = 'DOMString'">TDOMString.t</xsl:when>
    <xsl:when test="$type = 'EventListener'">TEventListener.t</xsl:when>
    <xsl:when test="$type = 'DOMImplementation'">TDOMImplementation.t</xsl:when>
    <xsl:when test="$invariant">T<xsl:value-of select="$type"/>.t</xsl:when>
    <xsl:otherwise>[&gt; `<xsl:value-of select="$type"/>] GdomeT.t</xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$nullable = 'yes'"> option</xsl:if>
</xsl:template>

<xsl:template name="gdomeNodeTypeOfType">
  <xsl:param name="type" select="''"/>
  <xsl:choose>
    <xsl:when test="$type = 'DocumentFragment'">DOCUMENT_FRAGMENT</xsl:when>
    <xsl:when test="$type = 'Document'">DOCUMENT</xsl:when>
    <xsl:when test="$type = 'Attr'">ATTRIBUTE</xsl:when>
    <xsl:when test="$type = 'Element'">ELEMENT</xsl:when>
    <xsl:when test="$type = 'Text'">TEXT</xsl:when>
    <xsl:when test="$type = 'Comment'">COMMENT</xsl:when>
    <xsl:when test="$type = 'CDATASection'">CDATA_SECTION</xsl:when>
    <xsl:when test="$type = 'DocumentType'">DOCUMENT_TYPE</xsl:when>
    <xsl:when test="$type = 'Notation'">NOTATION</xsl:when>
    <xsl:when test="$type = 'Entity'">ENTITY</xsl:when>
    <xsl:when test="$type = 'EntityReference'">ENTITY_REFERENCE</xsl:when>
    <xsl:when test="$type = 'ProcessingInstruction'">PROCESSING_INSTRUCTION</xsl:when>
    <xsl:otherwise>type</xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>

