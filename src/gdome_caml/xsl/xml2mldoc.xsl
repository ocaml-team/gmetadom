<?xml version="1.0"?>

<!--
 This file is part of GMetaDOM
 a generic bind package for the Document Object Model API.
 Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
               2001 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For more information, please visit the project home page
 http://gmetadom.sourceforge.net
 or send an email to <luca.padovani@cs.unibo.it>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="justifycation-string"
 select="'                                                                               '"/>
<xsl:variable name="comments-width" select="70"/>

<xsl:template name="justify">
 <xsl:param name="str" select="''"/>
 <xsl:param name="width" select="$comments-width"/>
 <xsl:param name="indent" select="0"/>
 <xsl:param name="inner-indent" select="0"/>
 <xsl:param name="first-line-indent" select="0"/>
 <xsl:variable name="strlen" select="string-length($str)"/>
 <xsl:choose>
  <xsl:when test="($strlen + $inner-indent + $first-line-indent) &lt; $comments-width">
   <xsl:value-of select="substring($justifycation-string,1,$indent)"/>
   <xsl:text>(* </xsl:text>
   <xsl:value-of select="substring($justifycation-string,1,$inner-indent + $first-line-indent)"/>
   <xsl:value-of select="$str"/>
   <xsl:value-of select="substring($justifycation-string,1,$comments-width - $strlen - $inner-indent - $first-line-indent)"/>
   <xsl:text> *)
</xsl:text>
  </xsl:when>
  <xsl:when test="substring($str,$width - $inner-indent - $first-line-indent,1) = ' '">
   <xsl:value-of select="substring($justifycation-string,1,$indent)"/>
   <xsl:text>(* </xsl:text>
   <xsl:value-of select="substring($justifycation-string,1,$inner-indent + $first-line-indent)"/>
   <xsl:value-of select="substring($str,1,$width - $inner-indent - $first-line-indent)"/>
   <xsl:value-of select="substring($justifycation-string,1,$comments-width - $width)"/>
   <xsl:text> *)
</xsl:text>
   <xsl:call-template name="justify">
    <xsl:with-param name="str" select="substring($str,$width - $inner-indent - $first-line-indent+1,$strlen - $width + $inner-indent + $first-line-indent)"/>
    <xsl:with-param name="indent" select="$indent"/>
    <xsl:with-param name="inner-indent" select="$inner-indent"/>
    <xsl:with-param name="first-line-indent" select="0"/>
   </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
   <xsl:call-template name="justify">
    <xsl:with-param name="str" select="$str"/>
    <xsl:with-param name="width" select="$width - 1"/>
    <xsl:with-param name="indent" select="$indent"/>
    <xsl:with-param name="inner-indent" select="$inner-indent"/>
    <xsl:with-param name="first-line-indent" select="$first-line-indent"/>
   </xsl:call-template>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template match="p">
 <xsl:apply-templates/>
 <xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="code">
 <xsl:text>[</xsl:text>
 <xsl:variable name="str">
  <xsl:value-of select="."/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$str = 'null'">None</xsl:when>
  <xsl:otherwise>
   <xsl:call-template name="ocamlTypeOfType">
    <xsl:with-param name="type" select="$str"/>
    <xsl:with-param name="isNullable" select="false()"/>
   </xsl:call-template>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:text>]</xsl:text>
</xsl:template>
 
<xsl:template name="descr">
 <xsl:param name="indent" select="0"/>
 <xsl:variable name="comment">
  <xsl:apply-templates select="p"/>
 </xsl:variable>
 <xsl:call-template name="justify">
  <xsl:with-param name="str" select="normalize-space($comment)"/>
  <xsl:with-param name="indent" select="$indent"/>
 </xsl:call-template>
</xsl:template>

<xsl:template name="parameters-doc">
 <xsl:param name="indent" select="0"/>
 <xsl:call-template name="justify">
  <xsl:with-param name="str" select="'Parameters:'"/>
  <xsl:with-param name="indent" select="$indent"/>
 </xsl:call-template>
 <xsl:for-each select="param">
  <xsl:variable name="comment">
   <xsl:value-of select="@name"/>
   <xsl:text>: </xsl:text>
   <xsl:apply-templates select="descr/p"/>
  </xsl:variable>
  <xsl:call-template name="justify">
   <xsl:with-param name="str" select="normalize-space($comment)"/>
   <xsl:with-param name="indent" select="$indent"/>
   <xsl:with-param name="inner-indent" select="3"/>
   <xsl:with-param name="first-line-indent" select="-2"/>
  </xsl:call-template>
 </xsl:for-each>
</xsl:template>

<xsl:template name="returns-doc">
 <xsl:param name="indent" select="0"/>
 <xsl:call-template name="justify">
  <xsl:with-param name="str" select="'Result:'"/>
  <xsl:with-param name="indent" select="$indent"/>
 </xsl:call-template>
 <xsl:variable name="comment">
  <xsl:apply-templates select="descr/p"/>
 </xsl:variable>
 <xsl:call-template name="justify">
  <xsl:with-param name="str" select="normalize-space($comment)"/>
  <xsl:with-param name="indent" select="$indent"/>
   <xsl:with-param name="inner-indent" select="1"/>
   <xsl:with-param name="first-line-indent" select="0"/>
 </xsl:call-template>
</xsl:template>

<xsl:template name="raises-doc">
 <xsl:param name="indent" select="0"/>
 <xsl:call-template name="justify">
  <xsl:with-param name="str" select="'Exceptions Raised:'"/>
  <xsl:with-param name="indent" select="$indent"/>
 </xsl:call-template>
 <xsl:for-each select="exception">
  <xsl:call-template name="justify">
   <xsl:with-param name="str" select="concat(@name,': ')"/>
   <xsl:with-param name="indent" select="$indent"/>
   <xsl:with-param name="inner-indent" select="3"/>
   <xsl:with-param name="first-line-indent" select="-2"/>
  </xsl:call-template>
  <xsl:variable name="comment">
   <xsl:apply-templates select="descr/p"/>
  </xsl:variable>
  <xsl:call-template name="justify">
   <xsl:with-param name="str" select="normalize-space($comment)"/>
   <xsl:with-param name="indent" select="$indent"/>
   <xsl:with-param name="inner-indent" select="3"/>
   <xsl:with-param name="first-line-indent" select="0"/>
  </xsl:call-template>
 </xsl:for-each>
</xsl:template>

<!-- The next two templates are an awful workaround for a bug of libxslt -->
<xsl:template match="descr" mode="indent0">
 <xsl:call-template name="descr">
  <xsl:with-param name="indent" select="0"/>
 </xsl:call-template>
</xsl:template>

<xsl:template match="descr" mode="indent3">
 <xsl:call-template name="descr">
  <xsl:with-param name="indent" select="3"/>
 </xsl:call-template>
</xsl:template>

<xsl:template match="parameters" mode="indent3">
 <xsl:call-template name="parameters-doc">
  <xsl:with-param name="indent" select="3"/>
 </xsl:call-template>
</xsl:template>

<xsl:template match="returns" mode="indent3">
 <xsl:call-template name="returns-doc">
  <xsl:with-param name="indent" select="3"/>
 </xsl:call-template>
</xsl:template>

<xsl:template match="getraises" mode="indent3">
 <xsl:call-template name="raises-doc">
  <xsl:with-param name="indent" select="3"/>
 </xsl:call-template>
</xsl:template>

<xsl:template match="setraises" mode="indent3">
 <xsl:call-template name="raises-doc">
  <xsl:with-param name="indent" select="3"/>
 </xsl:call-template>
</xsl:template>

<xsl:template match="raises" mode="indent3">
 <xsl:call-template name="raises-doc">
  <xsl:with-param name="indent" select="3"/>
 </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
