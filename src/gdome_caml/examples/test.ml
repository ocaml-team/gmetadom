open Gdome;;

let di = domImplementation ()

let doc = di#createDocumentFromURI ~uri:(Sys.argv.(1)) ()

let root = doc#get_documentElement

let old = (root#getAttribute (domString "hello-spank"))

let listener = eventListener (function x -> Printf.printf "hello\n")

let _ = root#addEventListener (domString "DOMAttrModified") listener false

let _ =
 root#setAttribute (domString "hello-spank")
  (domString (old#to_string ^ " and Aika"))
;;

let res = di#saveDocumentToFile ~doc ~name:"hi.xml" ~indent:true ()

let _ = Printf.printf "done!\n"
