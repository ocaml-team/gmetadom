
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external of_Event : [> `Event] GdomeT.t -> TMutationEvent.t = "ml_gdome_mevnt_of_evnt"

  
external get_relatedNode : this:[> `MutationEvent] GdomeT.t -> TNode.t option = "ml_gdome_mevnt_get_relatedNode"


external get_prevValue : this:[> `MutationEvent] GdomeT.t -> TDOMString.t option = "ml_gdome_mevnt_get_prevValue"


external get_newValue : this:[> `MutationEvent] GdomeT.t -> TDOMString.t option = "ml_gdome_mevnt_get_newValue"


external get_attrName : this:[> `MutationEvent] GdomeT.t -> TDOMString.t option = "ml_gdome_mevnt_get_attrName"


external get_attrChange : this:[> `MutationEvent] GdomeT.t -> int = "ml_gdome_mevnt_get_attrChange"


external initMutationEvent : this:[> `MutationEvent] GdomeT.t -> typeArg:TDOMString.t -> canBubbleArg:bool -> cancelableArg:bool -> relatedNodeArg:[> `Node] GdomeT.t option -> prevValueArg:TDOMString.t option -> newValueArg:TDOMString.t option -> attrNameArg:TDOMString.t option -> attrChangeArg:int option -> unit = "ml_gdome_mevnt_initMutationEvent" "ml_gdome_mevnt_initMutationEvent"