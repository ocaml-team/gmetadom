
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external get_type : this:[> `Event] GdomeT.t -> TDOMString.t = "ml_gdome_evnt_get_type"


external get_target : this:[> `Event] GdomeT.t -> TEventTarget.t = "ml_gdome_evnt_get_target"


external get_currentTarget : this:[> `Event] GdomeT.t -> TEventTarget.t = "ml_gdome_evnt_get_currentTarget"


external get_eventPhase : this:[> `Event] GdomeT.t -> int = "ml_gdome_evnt_get_eventPhase"


external get_bubbles : this:[> `Event] GdomeT.t -> bool = "ml_gdome_evnt_get_bubbles"


external get_cancelable : this:[> `Event] GdomeT.t -> bool = "ml_gdome_evnt_get_cancelable"


external get_timeStamp : this:[> `Event] GdomeT.t -> TDOMTimeStamp.t = "ml_gdome_evnt_get_timeStamp"


external stopPropagation : this:[> `Event] GdomeT.t -> unit = "ml_gdome_evnt_stopPropagation"
external preventDefault : this:[> `Event] GdomeT.t -> unit = "ml_gdome_evnt_preventDefault"
external initEvent : this:[> `Event] GdomeT.t -> eventTypeArg:TDOMString.t -> canBubbleArg:bool -> cancelableArg:bool -> unit = "ml_gdome_evnt_initEvent"