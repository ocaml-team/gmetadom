
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <gdome-events.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_mevnt_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeMutationEvent* obj_ = MutationEvent_val(v);
  g_assert(obj_ != NULL);
  gdome_mevnt_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_mevnt_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeMutationEvent* obj1_ = MutationEvent_val(v1);
  GdomeMutationEvent* obj2_ = MutationEvent_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeMutationEvent*
MutationEvent_val(value v)
{
  GdomeMutationEvent* res_ = *((GdomeMutationEvent**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_MutationEvent(GdomeMutationEvent* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Events/MutationEvent",
    ml_gdome_mevnt_finalize,
    ml_gdome_mevnt_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeMutationEvent*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeMutationEvent**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_mevnt_of_evnt(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeMutationEvent* obj_;
  
  obj_ = gdome_cast_mevnt((GdomeEvent*) Event_val(obj));
    
  if (obj_ == 0) throw_cast_exception("MutationEvent");
  gdome_mevnt_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "MutationEvent casting from Event");
  CAMLreturn(Val_MutationEvent(obj_));
}

    
value
ml_gdome_mevnt_get_relatedNode(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNode* res_;
  
  res_ = gdome_mevnt_relatedNode(MutationEvent_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "MutationEvent.get_relatedNode");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_mevnt_get_prevValue(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_mevnt_prevValue(MutationEvent_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "MutationEvent.get_prevValue");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}


value
ml_gdome_mevnt_get_newValue(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_mevnt_newValue(MutationEvent_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "MutationEvent.get_newValue");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}


value
ml_gdome_mevnt_get_attrName(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_mevnt_attrName(MutationEvent_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "MutationEvent.get_attrName");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}


value
ml_gdome_mevnt_get_attrChange(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  unsigned short res_;
  
  res_ = gdome_mevnt_attrChange(MutationEvent_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "MutationEvent.get_attrChange");
  CAMLreturn(Val_int(res_));
}


value
ml_gdome_mevnt_initMutationEvent(value self, value p_typeArg, value p_canBubbleArg, value p_cancelableArg, value p_relatedNodeArg, value p_prevValueArg, value p_newValueArg, value p_attrNameArg, value p_attrChangeArg)
{
  CAMLparam5(self, p_typeArg, p_canBubbleArg, p_cancelableArg, p_relatedNodeArg);
  CAMLxparam4(p_prevValueArg, p_newValueArg, p_attrNameArg, p_attrChangeArg);
      
  
  GdomeException exc_;
  
  GdomeDOMString* p_typeArg_ = DOMString_val(p_typeArg);
  GdomeDOMString* p_prevValueArg_ = ptr_val_option(p_prevValueArg, DOMString_val);
  GdomeDOMString* p_newValueArg_ = ptr_val_option(p_newValueArg, DOMString_val);
  GdomeDOMString* p_attrNameArg_ = ptr_val_option(p_attrNameArg, DOMString_val);gdome_mevnt_initMutationEvent(MutationEvent_val(self), p_typeArg_, Bool_val(p_canBubbleArg), Bool_val(p_cancelableArg), ptr_val_option(p_relatedNodeArg,Node_val), p_prevValueArg_, p_newValueArg_, p_attrNameArg_, Int_val(p_attrChangeArg), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "MutationEvent.initMutationEvent");
  CAMLreturn(Val_unit);
}


value
ml_gdome_mevnt_initMutationEvent_bytecode(value *argv, int argn)
{
  g_assert(argv != NULL);
  return ml_gdome_mevnt_initMutationEvent(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5], argv[6], argv[7], argv[8]);
}

