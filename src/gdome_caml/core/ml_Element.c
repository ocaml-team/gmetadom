
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_el_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeElement* obj_ = Element_val(v);
  g_assert(obj_ != NULL);
  gdome_el_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_el_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeElement* obj1_ = Element_val(v1);
  GdomeElement* obj2_ = Element_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeElement*
Element_val(value v)
{
  GdomeElement* res_ = *((GdomeElement**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_Element(GdomeElement* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/Element",
    ml_gdome_el_finalize,
    ml_gdome_el_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeElement*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeElement**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_el_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeElement* obj_;
  
  obj_ = gdome_cast_el((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("Element");
  gdome_el_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "Element casting from Node");
  CAMLreturn(Val_Element(obj_));
}

    
value
ml_gdome_el_get_tagName(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_el_tagName(Element_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Element.get_tagName");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_el_getAttribute(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeDOMString* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);
  value res__;
  res_ = gdome_el_getAttribute(Element_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.getAttribute");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_el_setAttribute(value self, value p_name, value p_value)
{
  CAMLparam3(self, p_name, p_value);
  
  GdomeException exc_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);
  GdomeDOMString* p_value_ = DOMString_val(p_value);gdome_el_setAttribute(Element_val(self), p_name_, p_value_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.setAttribute");
  CAMLreturn(Val_unit);
}


value
ml_gdome_el_removeAttribute(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);gdome_el_removeAttribute(Element_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.removeAttribute");
  CAMLreturn(Val_unit);
}


value
ml_gdome_el_getAttributeNode(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeAttr* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_el_getAttributeNode(Element_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.getAttributeNode");
  CAMLreturn(Val_option_ptr(res_,Val_Attr));
}


value
ml_gdome_el_setAttributeNode(value self, value p_newAttr)
{
  CAMLparam2(self, p_newAttr);
  
  GdomeException exc_;
  GdomeAttr* res_;
  res_ = gdome_el_setAttributeNode(Element_val(self), Attr_val(p_newAttr), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.setAttributeNode");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Attr(res_));
}


value
ml_gdome_el_removeAttributeNode(value self, value p_oldAttr)
{
  CAMLparam2(self, p_oldAttr);
  
  GdomeException exc_;
  GdomeAttr* res_;
  res_ = gdome_el_removeAttributeNode(Element_val(self), Attr_val(p_oldAttr), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.removeAttributeNode");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Attr(res_));
}


value
ml_gdome_el_getElementsByTagName(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeNodeList* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_el_getElementsByTagName(Element_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.getElementsByTagName");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NodeList(res_));
}


value
ml_gdome_el_getAttributeNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeDOMString* res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);
  value res__;
  res_ = gdome_el_getAttributeNS(Element_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.getAttributeNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_el_setAttributeNS(value self, value p_namespaceURI, value p_qualifiedName, value p_value)
{
  CAMLparam4(self, p_namespaceURI, p_qualifiedName, p_value);
  
  GdomeException exc_;
  
  GdomeDOMString* p_namespaceURI_ = ptr_val_option(p_namespaceURI, DOMString_val);
  GdomeDOMString* p_qualifiedName_ = DOMString_val(p_qualifiedName);
  GdomeDOMString* p_value_ = DOMString_val(p_value);gdome_el_setAttributeNS(Element_val(self), p_namespaceURI_, p_qualifiedName_, p_value_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.setAttributeNS");
  CAMLreturn(Val_unit);
}


value
ml_gdome_el_removeAttributeNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);gdome_el_removeAttributeNS(Element_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.removeAttributeNS");
  CAMLreturn(Val_unit);
}


value
ml_gdome_el_getAttributeNodeNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeAttr* res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);res_ = gdome_el_getAttributeNodeNS(Element_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.getAttributeNodeNS");
  CAMLreturn(Val_option_ptr(res_,Val_Attr));
}


value
ml_gdome_el_setAttributeNodeNS(value self, value p_newAttr)
{
  CAMLparam2(self, p_newAttr);
  
  GdomeException exc_;
  GdomeAttr* res_;
  res_ = gdome_el_setAttributeNodeNS(Element_val(self), Attr_val(p_newAttr), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.setAttributeNodeNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Attr(res_));
}


value
ml_gdome_el_getElementsByTagNameNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeNodeList* res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);res_ = gdome_el_getElementsByTagNameNS(Element_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.getElementsByTagNameNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NodeList(res_));
}


value
ml_gdome_el_hasAttribute(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeBoolean res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_el_hasAttribute(Element_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.hasAttribute");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_el_hasAttributeNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeBoolean res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);res_ = gdome_el_hasAttributeNS(Element_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Element.hasAttributeNS");
  CAMLreturn(Val_bool(res_));
}

