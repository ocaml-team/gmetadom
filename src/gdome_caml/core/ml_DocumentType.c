
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_dt_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeDocumentType* obj_ = DocumentType_val(v);
  g_assert(obj_ != NULL);
  gdome_dt_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_dt_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeDocumentType* obj1_ = DocumentType_val(v1);
  GdomeDocumentType* obj2_ = DocumentType_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeDocumentType*
DocumentType_val(value v)
{
  GdomeDocumentType* res_ = *((GdomeDocumentType**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_DocumentType(GdomeDocumentType* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/DocumentType",
    ml_gdome_dt_finalize,
    ml_gdome_dt_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeDocumentType*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeDocumentType**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_dt_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeDocumentType* obj_;
  
  obj_ = gdome_cast_dt((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("DocumentType");
  gdome_dt_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType casting from Node");
  CAMLreturn(Val_DocumentType(obj_));
}

    
value
ml_gdome_dt_get_name(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_dt_name(DocumentType_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType.get_name");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_dt_get_entities(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNamedNodeMap* res_;
  
  res_ = gdome_dt_entities(DocumentType_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType.get_entities");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NamedNodeMap(res_));
}


value
ml_gdome_dt_get_notations(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNamedNodeMap* res_;
  
  res_ = gdome_dt_notations(DocumentType_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType.get_notations");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NamedNodeMap(res_));
}


value
ml_gdome_dt_get_publicId(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_dt_publicId(DocumentType_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType.get_publicId");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_dt_get_systemId(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_dt_systemId(DocumentType_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType.get_systemId");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_dt_get_internalSubset(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_dt_internalSubset(DocumentType_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "DocumentType.get_internalSubset");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}

