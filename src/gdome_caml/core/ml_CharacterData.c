
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_cd_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeCharacterData* obj_ = CharacterData_val(v);
  g_assert(obj_ != NULL);
  gdome_cd_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_cd_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeCharacterData* obj1_ = CharacterData_val(v1);
  GdomeCharacterData* obj2_ = CharacterData_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeCharacterData*
CharacterData_val(value v)
{
  GdomeCharacterData* res_ = *((GdomeCharacterData**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_CharacterData(GdomeCharacterData* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/CharacterData",
    ml_gdome_cd_finalize,
    ml_gdome_cd_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeCharacterData*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeCharacterData**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_cd_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeCharacterData* obj_;
  
  obj_ = gdome_cast_cd((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("CharacterData");
  gdome_cd_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "CharacterData casting from Node");
  CAMLreturn(Val_CharacterData(obj_));
}

    
value
ml_gdome_cd_get_data(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_cd_data(CharacterData_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "CharacterData.get_data");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}

value
ml_gdome_cd_set_data(value self, value p_v)
{
  CAMLparam2(self, p_v);
  GdomeException exc_;
  
  GdomeDOMString* p_v_ = DOMString_val(p_v);
  gdome_cd_set_data(CharacterData_val(self), p_v_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "CharacterData.set_data");
  CAMLreturn(Val_unit);
}


value
ml_gdome_cd_get_length(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  unsigned long res_;
  
  res_ = gdome_cd_length(CharacterData_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "CharacterData.get_length");
  CAMLreturn(Val_int(res_));
}


value
ml_gdome_cd_substringData(value self, value p_offset, value p_count)
{
  CAMLparam3(self, p_offset, p_count);
  
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  res_ = gdome_cd_substringData(CharacterData_val(self), Int_val(p_offset), Int_val(p_count), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "CharacterData.substringData");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_cd_appendData(value self, value p_arg)
{
  CAMLparam2(self, p_arg);
  
  GdomeException exc_;
  
  GdomeDOMString* p_arg_ = DOMString_val(p_arg);gdome_cd_appendData(CharacterData_val(self), p_arg_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "CharacterData.appendData");
  CAMLreturn(Val_unit);
}


value
ml_gdome_cd_insertData(value self, value p_offset, value p_arg)
{
  CAMLparam3(self, p_offset, p_arg);
  
  GdomeException exc_;
  
  GdomeDOMString* p_arg_ = DOMString_val(p_arg);gdome_cd_insertData(CharacterData_val(self), Int_val(p_offset), p_arg_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "CharacterData.insertData");
  CAMLreturn(Val_unit);
}


value
ml_gdome_cd_deleteData(value self, value p_offset, value p_count)
{
  CAMLparam3(self, p_offset, p_count);
  
  GdomeException exc_;
  gdome_cd_deleteData(CharacterData_val(self), Int_val(p_offset), Int_val(p_count), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "CharacterData.deleteData");
  CAMLreturn(Val_unit);
}


value
ml_gdome_cd_replaceData(value self, value p_offset, value p_count, value p_arg)
{
  CAMLparam4(self, p_offset, p_count, p_arg);
  
  GdomeException exc_;
  
  GdomeDOMString* p_arg_ = DOMString_val(p_arg);gdome_cd_replaceData(CharacterData_val(self), Int_val(p_offset), Int_val(p_count), p_arg_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "CharacterData.replaceData");
  CAMLreturn(Val_unit);
}

