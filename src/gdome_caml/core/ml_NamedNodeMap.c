
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_nnm_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeNamedNodeMap* obj_ = NamedNodeMap_val(v);
  g_assert(obj_ != NULL);
  gdome_nnm_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_nnm_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeNamedNodeMap* obj1_ = NamedNodeMap_val(v1);
  GdomeNamedNodeMap* obj2_ = NamedNodeMap_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeNamedNodeMap*
NamedNodeMap_val(value v)
{
  GdomeNamedNodeMap* res_ = *((GdomeNamedNodeMap**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_NamedNodeMap(GdomeNamedNodeMap* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/NamedNodeMap",
    ml_gdome_nnm_finalize,
    ml_gdome_nnm_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeNamedNodeMap*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeNamedNodeMap**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_nnm_get_length(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  unsigned long res_;
  
  res_ = gdome_nnm_length(NamedNodeMap_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.get_length");
  CAMLreturn(Val_int(res_));
}


value
ml_gdome_nnm_getNamedItem(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeNode* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_nnm_getNamedItem(NamedNodeMap_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.getNamedItem");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_nnm_setNamedItem(value self, value p_arg)
{
  CAMLparam2(self, p_arg);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_nnm_setNamedItem(NamedNodeMap_val(self), Node_val(p_arg), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.setNamedItem");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_nnm_removeNamedItem(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeNode* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_nnm_removeNamedItem(NamedNodeMap_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.removeNamedItem");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_nnm_item(value self, value p_index)
{
  CAMLparam2(self, p_index);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_nnm_item(NamedNodeMap_val(self), Int_val(p_index), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.item");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_nnm_getNamedItemNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeNode* res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);res_ = gdome_nnm_getNamedItemNS(NamedNodeMap_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.getNamedItemNS");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_nnm_setNamedItemNS(value self, value p_arg)
{
  CAMLparam2(self, p_arg);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_nnm_setNamedItemNS(NamedNodeMap_val(self), Node_val(p_arg), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.setNamedItemNS");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_nnm_removeNamedItemNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeNode* res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);res_ = gdome_nnm_removeNamedItemNS(NamedNodeMap_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "NamedNodeMap.removeNamedItemNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}

