
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_n_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeNode* obj_ = Node_val(v);
  g_assert(obj_ != NULL);
  gdome_n_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_n_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeNode* obj1_ = Node_val(v1);
  GdomeNode* obj2_ = Node_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeNode*
Node_val(value v)
{
  GdomeNode* res_ = *((GdomeNode**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_Node(GdomeNode* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/Node",
    ml_gdome_n_finalize,
    ml_gdome_n_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeNode*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeNode**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_n_isSameNode(value self, value p_other)
{
  CAMLparam2(self, p_other);
  GdomeNode* obj1_ = Node_val(self);
  GdomeNode* obj2_ = Node_val(p_other);
  CAMLreturn(Val_bool(obj1_ == obj2_));
}

value
ml_gdome_n_get_nodeName(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_n_nodeName(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_nodeName");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMString(res_));
}


value
ml_gdome_n_get_nodeValue(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_n_nodeValue(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_nodeValue");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}

value
ml_gdome_n_set_nodeValue(value self, value p_v)
{
  CAMLparam2(self, p_v);
  GdomeException exc_;
  
  GdomeDOMString* p_v_ = ptr_val_option(p_v, DOMString_val);
  gdome_n_set_nodeValue(Node_val(self), p_v_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.set_nodeValue");
  CAMLreturn(Val_unit);
}


value
ml_gdome_n_get_nodeType(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  unsigned short res_;
  
  res_ = gdome_n_nodeType(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_nodeType");
  CAMLreturn(Val_int(res_));
}


value
ml_gdome_n_get_parentNode(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNode* res_;
  
  res_ = gdome_n_parentNode(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_parentNode");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_n_get_childNodes(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNodeList* res_;
  
  res_ = gdome_n_childNodes(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_childNodes");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NodeList(res_));
}


value
ml_gdome_n_get_firstChild(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNode* res_;
  
  res_ = gdome_n_firstChild(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_firstChild");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_n_get_lastChild(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNode* res_;
  
  res_ = gdome_n_lastChild(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_lastChild");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_n_get_previousSibling(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNode* res_;
  
  res_ = gdome_n_previousSibling(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_previousSibling");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_n_get_nextSibling(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNode* res_;
  
  res_ = gdome_n_nextSibling(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_nextSibling");
  CAMLreturn(Val_option_ptr(res_,Val_Node));
}


value
ml_gdome_n_get_attributes(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeNamedNodeMap* res_;
  
  res_ = gdome_n_attributes(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_attributes");
  CAMLreturn(Val_option_ptr(res_,Val_NamedNodeMap));
}


value
ml_gdome_n_get_ownerDocument(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDocument* res_;
  
  res_ = gdome_n_ownerDocument(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_ownerDocument");
  CAMLreturn(Val_option_ptr(res_,Val_Document));
}


value
ml_gdome_n_get_namespaceURI(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_n_namespaceURI(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_namespaceURI");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}


value
ml_gdome_n_get_prefix(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_n_prefix(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_prefix");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}

value
ml_gdome_n_set_prefix(value self, value p_v)
{
  CAMLparam2(self, p_v);
  GdomeException exc_;
  
  GdomeDOMString* p_v_ = ptr_val_option(p_v, DOMString_val);
  gdome_n_set_prefix(Node_val(self), p_v_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.set_prefix");
  CAMLreturn(Val_unit);
}


value
ml_gdome_n_get_localName(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMString* res_;
  
  value res__;
  
  res_ = gdome_n_localName(Node_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Node.get_localName");
  CAMLreturn(Val_option_ptr(res_,Val_DOMString));
}


value
ml_gdome_n_insertBefore(value self, value p_newChild, value p_refChild)
{
  CAMLparam3(self, p_newChild, p_refChild);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_n_insertBefore(Node_val(self), Node_val(p_newChild), ptr_val_option(p_refChild,Node_val), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.insertBefore");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_n_replaceChild(value self, value p_newChild, value p_oldChild)
{
  CAMLparam3(self, p_newChild, p_oldChild);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_n_replaceChild(Node_val(self), Node_val(p_newChild), Node_val(p_oldChild), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.replaceChild");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_n_removeChild(value self, value p_oldChild)
{
  CAMLparam2(self, p_oldChild);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_n_removeChild(Node_val(self), Node_val(p_oldChild), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.removeChild");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_n_appendChild(value self, value p_newChild)
{
  CAMLparam2(self, p_newChild);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_n_appendChild(Node_val(self), Node_val(p_newChild), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.appendChild");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_n_hasChildNodes(value self)
{
  CAMLparam1(self);
  
  GdomeException exc_;
  GdomeBoolean res_;
  res_ = gdome_n_hasChildNodes(Node_val(self), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.hasChildNodes");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_n_cloneNode(value self, value p_deep)
{
  CAMLparam2(self, p_deep);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_n_cloneNode(Node_val(self), Bool_val(p_deep), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.cloneNode");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_n_normalize(value self)
{
  CAMLparam1(self);
  
  GdomeException exc_;
  gdome_n_normalize(Node_val(self), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.normalize");
  CAMLreturn(Val_unit);
}


value
ml_gdome_n_isSupported(value self, value p_feature, value p_version)
{
  CAMLparam3(self, p_feature, p_version);
  
  GdomeException exc_;
  GdomeBoolean res_;
  
  GdomeDOMString* p_feature_ = DOMString_val(p_feature);
  GdomeDOMString* p_version_ = DOMString_val(p_version);res_ = gdome_n_isSupported(Node_val(self), p_feature_, p_version_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.isSupported");
  CAMLreturn(Val_bool(res_));
}


value
ml_gdome_n_hasAttributes(value self)
{
  CAMLparam1(self);
  
  GdomeException exc_;
  GdomeBoolean res_;
  res_ = gdome_n_hasAttributes(Node_val(self), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Node.hasAttributes");
  CAMLreturn(Val_bool(res_));
}

