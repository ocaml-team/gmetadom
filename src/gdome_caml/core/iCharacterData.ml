
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external of_Node : [> `Node] GdomeT.t -> TCharacterData.t = "ml_gdome_cd_of_n"

  
external get_data : this:[> `CharacterData] GdomeT.t -> TDOMString.t = "ml_gdome_cd_get_data"


external set_data : this:[> `CharacterData] GdomeT.t -> value:TDOMString.t -> unit = "ml_gdome_cd_set_data"

external get_length : this:[> `CharacterData] GdomeT.t -> int = "ml_gdome_cd_get_length"


external substringData : this:[> `CharacterData] GdomeT.t -> offset:int -> count:int -> TDOMString.t = "ml_gdome_cd_substringData"
external appendData : this:[> `CharacterData] GdomeT.t -> arg:TDOMString.t -> unit = "ml_gdome_cd_appendData"
external insertData : this:[> `CharacterData] GdomeT.t -> offset:int -> arg:TDOMString.t -> unit = "ml_gdome_cd_insertData"
external deleteData : this:[> `CharacterData] GdomeT.t -> offset:int -> count:int -> unit = "ml_gdome_cd_deleteData"
external replaceData : this:[> `CharacterData] GdomeT.t -> offset:int -> count:int -> arg:TDOMString.t -> unit = "ml_gdome_cd_replaceData"