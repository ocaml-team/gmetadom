
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external get_nodeName : this:[> `Node] GdomeT.t -> TDOMString.t = "ml_gdome_n_get_nodeName"


external get_nodeValue : this:[> `Node] GdomeT.t -> TDOMString.t option = "ml_gdome_n_get_nodeValue"


external set_nodeValue : this:[> `Node] GdomeT.t -> value:TDOMString.t option -> unit = "ml_gdome_n_set_nodeValue"

external get_nodeType : this:[> `Node] GdomeT.t -> GdomeNodeTypeT.t = "ml_gdome_n_get_nodeType"


external get_parentNode : this:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_n_get_parentNode"


external get_childNodes : this:[> `Node] GdomeT.t -> TNodeList.t = "ml_gdome_n_get_childNodes"


external get_firstChild : this:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_n_get_firstChild"


external get_lastChild : this:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_n_get_lastChild"


external get_previousSibling : this:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_n_get_previousSibling"


external get_nextSibling : this:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_n_get_nextSibling"


external get_attributes : this:[> `Node] GdomeT.t -> TNamedNodeMap.t option = "ml_gdome_n_get_attributes"


external get_ownerDocument : this:[> `Node] GdomeT.t -> TDocument.t option = "ml_gdome_n_get_ownerDocument"


external get_namespaceURI : this:[> `Node] GdomeT.t -> TDOMString.t option = "ml_gdome_n_get_namespaceURI"


external get_prefix : this:[> `Node] GdomeT.t -> TDOMString.t option = "ml_gdome_n_get_prefix"


external set_prefix : this:[> `Node] GdomeT.t -> value:TDOMString.t option -> unit = "ml_gdome_n_set_prefix"

external get_localName : this:[> `Node] GdomeT.t -> TDOMString.t option = "ml_gdome_n_get_localName"


external isSameNode : this:[> `Node] GdomeT.t -> [> `Node] GdomeT.t -> bool = "ml_gdome_n_isSameNode"

external insertBefore : this:[> `Node] GdomeT.t -> newChild:[> `Node] GdomeT.t -> refChild:[> `Node] GdomeT.t option -> TNode.t = "ml_gdome_n_insertBefore"
external replaceChild : this:[> `Node] GdomeT.t -> newChild:[> `Node] GdomeT.t -> oldChild:[> `Node] GdomeT.t -> TNode.t = "ml_gdome_n_replaceChild"
external removeChild : this:[> `Node] GdomeT.t -> oldChild:[> `Node] GdomeT.t -> TNode.t = "ml_gdome_n_removeChild"
external appendChild : this:[> `Node] GdomeT.t -> newChild:[> `Node] GdomeT.t -> TNode.t = "ml_gdome_n_appendChild"
external hasChildNodes : this:[> `Node] GdomeT.t -> bool = "ml_gdome_n_hasChildNodes"
external cloneNode : this:[> `Node] GdomeT.t -> deep:bool -> TNode.t = "ml_gdome_n_cloneNode"
external normalize : this:[> `Node] GdomeT.t -> unit = "ml_gdome_n_normalize"
external isSupported : this:[> `Node] GdomeT.t -> feature:TDOMString.t -> version:TDOMString.t -> bool = "ml_gdome_n_isSupported"
external hasAttributes : this:[> `Node] GdomeT.t -> bool = "ml_gdome_n_hasAttributes"