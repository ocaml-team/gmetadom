
/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 */

#include <assert.h>
#include <gdome.h>

#include <caml/memory.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_doc_finalize(value v)
{
  GdomeException exc_ = 0;
  GdomeDocument* obj_ = Document_val(v);
  g_assert(obj_ != NULL);
  gdome_doc_unref(obj_, &exc_);
  assert(exc_ == 0);
}

static int
ml_gdome_doc_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeDocument* obj1_ = Document_val(v1);
  GdomeDocument* obj2_ = Document_val(v2);
  CAMLreturn((int) (obj1_ - obj2_));
}

GdomeDocument*
Document_val(value v)
{
  GdomeDocument* res_ = *((GdomeDocument**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
Val_Document(GdomeDocument* obj)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/Core/Document",
    ml_gdome_doc_finalize,
    ml_gdome_doc_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeDocument*), 0, 1);
  g_assert(obj != NULL);
  *((GdomeDocument**) Data_custom_val(v)) = obj;

  return v;
}


value
ml_gdome_doc_of_n(value obj)
{
  CAMLparam1(obj);
  GdomeException exc_;
  GdomeDocument* obj_;
  
  obj_ = gdome_cast_doc((GdomeNode*) Node_val(obj));
    
  if (obj_ == 0) throw_cast_exception("Document");
  gdome_doc_ref(obj_, &exc_);
  if (exc_ != 0) throw_exception(exc_, "Document casting from Node");
  CAMLreturn(Val_Document(obj_));
}

    
value
ml_gdome_doc_get_doctype(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDocumentType* res_;
  
  res_ = gdome_doc_doctype(Document_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Document.get_doctype");
  CAMLreturn(Val_option_ptr(res_,Val_DocumentType));
}


value
ml_gdome_doc_get_implementation(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeDOMImplementation* res_;
  
  res_ = gdome_doc_implementation(Document_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Document.get_implementation");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DOMImplementation(res_));
}


value
ml_gdome_doc_get_documentElement(value self)
{
  CAMLparam1(self);
  GdomeException exc_;
  GdomeElement* res_;
  
  res_ = gdome_doc_documentElement(Document_val(self), &exc_);
  if (exc_ != 0) throw_exception(exc_, "Document.get_documentElement");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Element(res_));
}


value
ml_gdome_doc_createElement(value self, value p_tagName)
{
  CAMLparam2(self, p_tagName);
  
  GdomeException exc_;
  GdomeElement* res_;
  
  GdomeDOMString* p_tagName_ = DOMString_val(p_tagName);res_ = gdome_doc_createElement(Document_val(self), p_tagName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createElement");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Element(res_));
}


value
ml_gdome_doc_createDocumentFragment(value self)
{
  CAMLparam1(self);
  
  GdomeException exc_;
  GdomeDocumentFragment* res_;
  res_ = gdome_doc_createDocumentFragment(Document_val(self), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createDocumentFragment");
  g_assert(res_ != NULL);
  CAMLreturn(Val_DocumentFragment(res_));
}


value
ml_gdome_doc_createTextNode(value self, value p_data)
{
  CAMLparam2(self, p_data);
  
  GdomeException exc_;
  GdomeText* res_;
  
  GdomeDOMString* p_data_ = DOMString_val(p_data);res_ = gdome_doc_createTextNode(Document_val(self), p_data_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createTextNode");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Text(res_));
}


value
ml_gdome_doc_createComment(value self, value p_data)
{
  CAMLparam2(self, p_data);
  
  GdomeException exc_;
  GdomeComment* res_;
  
  GdomeDOMString* p_data_ = DOMString_val(p_data);res_ = gdome_doc_createComment(Document_val(self), p_data_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createComment");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Comment(res_));
}


value
ml_gdome_doc_createCDATASection(value self, value p_data)
{
  CAMLparam2(self, p_data);
  
  GdomeException exc_;
  GdomeCDATASection* res_;
  
  GdomeDOMString* p_data_ = DOMString_val(p_data);res_ = gdome_doc_createCDATASection(Document_val(self), p_data_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createCDATASection");
  g_assert(res_ != NULL);
  CAMLreturn(Val_CDATASection(res_));
}


value
ml_gdome_doc_createProcessingInstruction(value self, value p_target, value p_data)
{
  CAMLparam3(self, p_target, p_data);
  
  GdomeException exc_;
  GdomeProcessingInstruction* res_;
  
  GdomeDOMString* p_target_ = DOMString_val(p_target);
  GdomeDOMString* p_data_ = DOMString_val(p_data);res_ = gdome_doc_createProcessingInstruction(Document_val(self), p_target_, p_data_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createProcessingInstruction");
  g_assert(res_ != NULL);
  CAMLreturn(Val_ProcessingInstruction(res_));
}


value
ml_gdome_doc_createAttribute(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeAttr* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_doc_createAttribute(Document_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createAttribute");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Attr(res_));
}


value
ml_gdome_doc_createEntityReference(value self, value p_name)
{
  CAMLparam2(self, p_name);
  
  GdomeException exc_;
  GdomeEntityReference* res_;
  
  GdomeDOMString* p_name_ = DOMString_val(p_name);res_ = gdome_doc_createEntityReference(Document_val(self), p_name_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createEntityReference");
  g_assert(res_ != NULL);
  CAMLreturn(Val_EntityReference(res_));
}


value
ml_gdome_doc_getElementsByTagName(value self, value p_tagname)
{
  CAMLparam2(self, p_tagname);
  
  GdomeException exc_;
  GdomeNodeList* res_;
  
  GdomeDOMString* p_tagname_ = DOMString_val(p_tagname);res_ = gdome_doc_getElementsByTagName(Document_val(self), p_tagname_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.getElementsByTagName");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NodeList(res_));
}


value
ml_gdome_doc_importNode(value self, value p_importedNode, value p_deep)
{
  CAMLparam3(self, p_importedNode, p_deep);
  
  GdomeException exc_;
  GdomeNode* res_;
  res_ = gdome_doc_importNode(Document_val(self), Node_val(p_importedNode), Bool_val(p_deep), &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.importNode");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Node(res_));
}


value
ml_gdome_doc_createElementNS(value self, value p_namespaceURI, value p_qualifiedName)
{
  CAMLparam3(self, p_namespaceURI, p_qualifiedName);
  
  GdomeException exc_;
  GdomeElement* res_;
  
  GdomeDOMString* p_namespaceURI_ = ptr_val_option(p_namespaceURI, DOMString_val);
  GdomeDOMString* p_qualifiedName_ = DOMString_val(p_qualifiedName);res_ = gdome_doc_createElementNS(Document_val(self), p_namespaceURI_, p_qualifiedName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createElementNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Element(res_));
}


value
ml_gdome_doc_createAttributeNS(value self, value p_namespaceURI, value p_qualifiedName)
{
  CAMLparam3(self, p_namespaceURI, p_qualifiedName);
  
  GdomeException exc_;
  GdomeAttr* res_;
  
  GdomeDOMString* p_namespaceURI_ = ptr_val_option(p_namespaceURI, DOMString_val);
  GdomeDOMString* p_qualifiedName_ = DOMString_val(p_qualifiedName);res_ = gdome_doc_createAttributeNS(Document_val(self), p_namespaceURI_, p_qualifiedName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.createAttributeNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_Attr(res_));
}


value
ml_gdome_doc_getElementsByTagNameNS(value self, value p_namespaceURI, value p_localName)
{
  CAMLparam3(self, p_namespaceURI, p_localName);
  
  GdomeException exc_;
  GdomeNodeList* res_;
  
  GdomeDOMString* p_namespaceURI_ = DOMString_val(p_namespaceURI);
  GdomeDOMString* p_localName_ = DOMString_val(p_localName);res_ = gdome_doc_getElementsByTagNameNS(Document_val(self), p_namespaceURI_, p_localName_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.getElementsByTagNameNS");
  g_assert(res_ != NULL);
  CAMLreturn(Val_NodeList(res_));
}


value
ml_gdome_doc_getElementById(value self, value p_elementId)
{
  CAMLparam2(self, p_elementId);
  
  GdomeException exc_;
  GdomeElement* res_;
  
  GdomeDOMString* p_elementId_ = DOMString_val(p_elementId);res_ = gdome_doc_getElementById(Document_val(self), p_elementId_, &exc_);
  
  if (exc_ != 0) throw_exception(exc_, "Document.getElementById");
  CAMLreturn(Val_option_ptr(res_,Val_Element));
}

