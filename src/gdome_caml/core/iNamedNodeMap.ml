
(* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002  Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 *
 * DO NOT EDIT: this file has been generated automatically by GMetaDOM
 *)


external get_length : this:[> `NamedNodeMap] GdomeT.t -> int = "ml_gdome_nnm_get_length"


external getNamedItem : this:[> `NamedNodeMap] GdomeT.t -> name:TDOMString.t -> TNode.t option = "ml_gdome_nnm_getNamedItem"
external setNamedItem : this:[> `NamedNodeMap] GdomeT.t -> arg:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_nnm_setNamedItem"
external removeNamedItem : this:[> `NamedNodeMap] GdomeT.t -> name:TDOMString.t -> TNode.t = "ml_gdome_nnm_removeNamedItem"
external item : this:[> `NamedNodeMap] GdomeT.t -> index:int -> TNode.t option = "ml_gdome_nnm_item"
external getNamedItemNS : this:[> `NamedNodeMap] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> TNode.t option = "ml_gdome_nnm_getNamedItemNS"
external setNamedItemNS : this:[> `NamedNodeMap] GdomeT.t -> arg:[> `Node] GdomeT.t -> TNode.t option = "ml_gdome_nnm_setNamedItemNS"
external removeNamedItemNS : this:[> `NamedNodeMap] GdomeT.t -> namespaceURI:TDOMString.t -> localName:TDOMString.t -> TNode.t = "ml_gdome_nnm_removeNamedItemNS"