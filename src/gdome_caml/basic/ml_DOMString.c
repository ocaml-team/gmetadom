/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <assert.h>
#include <gdome.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include "mlgdomevalue.h"

static void
ml_gdome_str_finalize(value v)
{
  GdomeDOMString* obj_ = DOMString_val(v);
  g_assert(obj_ != NULL);
  gdome_str_unref(obj_);
}

static int
ml_gdome_str_compare(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeDOMString* str1 = DOMString_val(v1);
  GdomeDOMString* str2 = DOMString_val(v2);
  CAMLreturn(Val_int(strcmp(str1->str,str2->str)));
}

value
Val_DOMString(GdomeDOMString* str)
{
  static struct custom_operations ops = {
    "http://gmetadom.sourceforge.net/gdome_caml/DOMString",
    ml_gdome_str_finalize,
    ml_gdome_str_compare,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
  };
  
  value v = alloc_custom(&ops, sizeof(GdomeDOMString*), 0, 1);
  g_assert(str != NULL);
  *((GdomeDOMString**) Data_custom_val(v)) = str;

  return v;
}

GdomeDOMString*
DOMString_val(value v)
{
  GdomeDOMString* res_ = *((GdomeDOMString**) Data_custom_val(v));
  g_assert(res_ != NULL);
  return res_;
}

value
ml_gdome_DOMString_of_string(value str)
{
  return Val_DOMString(gdome_str_mkref_dup(String_val(str)));
}

value
ml_gdome_DOMString_to_string(value self)
{
  CAMLparam1(self);
  CAMLreturn(copy_string(DOMString_val(self)->str));
}

value
ml_gdome_DOMString_equals(value v1, value v2)
{
  CAMLparam2(v1,v2); 
  GdomeDOMString* str1 = DOMString_val(v1);
  GdomeDOMString* str2 = DOMString_val(v2);
  if (v1 == v2) CAMLreturn(Val_bool(1));
  else
     CAMLreturn(Val_bool(gdome_str_equal(str1,str2)));
}
