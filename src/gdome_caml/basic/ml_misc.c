/* This file is part of GMetaDOM
 * a generic bind package for the Document Object Model API.
 * Copyright (C) 2001-2002 Luca Padovani <luca.padovani@cs.unibo.it>
 *               2002 Claudio Sacerdoti Coen <sacerdot@cs.unibo.it>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, please visit the project home page
 * http://gmetadom.sourceforge.net
 * or send an email to <luca.padovani@cs.unibo.it>
 */

#include <assert.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include "mlgdomevalue.h"

value
mlgdome_some(value v)
{
  CAMLparam1(v);
  value ret = alloc_small(1,0);
  Field(ret,0) = v;
  CAMLreturn(ret);
}

void
throw_exception(int exc, const char* msg)
{
  value* excp;
  value res;
  assert(msg != NULL);
  res = alloc_tuple(2);
  Store_field(res,0,Val_int(exc));
  Store_field(res,1,copy_string(msg));
  excp = caml_named_value("DOMException");
  assert(excp != NULL);
  raise_with_arg(*excp, res);
}

void
throw_impl_exception(const char* msg)
{
  value* excp;
  assert(msg != NULL);
  excp = caml_named_value("DOMImplException");
  assert(excp != NULL);
  raise_with_arg(*excp, copy_string(msg));
}

void
throw_cast_exception(const char* msg)
{
  value* excp;
  assert(msg != NULL);
  excp = caml_named_value("DOMCastException");
  assert(excp != NULL);
  raise_with_arg(*excp, copy_string(msg));
}

