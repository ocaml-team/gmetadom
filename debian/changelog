gmetadom (0.2.6-9) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + libgdome2-cpp-smart0v5: Add Multi-Arch: same.

 -- Stéphane Glondu <glondu@debian.org>  Tue, 15 Aug 2023 10:13:48 +0200

gmetadom (0.2.6-8) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.6.2
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Sat, 12 Aug 2023 07:32:27 +0200

gmetadom (0.2.6-7) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Bump debhelper compat to 10 and switch to dh
  * Rebuild against GCC 7 (Closes: #871277)
  * Update Vcs-*

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders (Closes: #869324)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 08 Aug 2017 12:24:27 +0200

gmetadom (0.2.6-6.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename binary packages for g++ 5 transition (see #791052)

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 08 Aug 2015 14:02:50 +0100

gmetadom (0.2.6-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use dh-autoreconf to build on newer architectures (Closes: 753334)

 -- Wookey <wookey@debian.org>  Tue, 14 Oct 2014 14:12:30 +0000

gmetadom (0.2.6-6) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Remove Stefano from Uploaders
  * Switch source package format to 3.0 (quilt)

  [ Steve Langasek ]
  * debian/rules: clean the dependency_libs out of .la files at build
    time, per Policy 10.2 (Closes: #620614)

  [ Colin Watson ]
  * Rearrange link order to work with 'ld --as-needed'
    (LP: #771037, Closes: #627428)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 31 May 2011 10:39:12 +0200

gmetadom (0.2.6-5) unstable; urgency=low

  * debian/rules: fix include order to ensure dh-ocaml deps for
    ocaml-base-nox are actually generated (Closes: #569391)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 16 Feb 2010 10:19:04 +0100

gmetadom (0.2.6-4) unstable; urgency=low

  * Use dh-ocaml 0.9.1 features
  * Add myself to Uploaders
  * Upgrade Standards-Version to 3.8.3 (section ocaml, README.source)
  * Add debian/gbp.conf to use pristine-tar

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 19 Dec 2009 12:41:28 +0100

gmetadom (0.2.6-3) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * rebuild against ocaml 3.11, bump versioned build-deps accordingly
  * bump debhelper compatibility level to 7
  * debian/control
    - bump standard version to 3.8.0, no changes needed
    - drop obsolete strict dependencies
    - set maintainer to d-o-m, myself as an uploader
  * debian/rules: use ocaml.mk as a CDBS "rules" snippet
  * debian/*.in: use specific substitution variables instead of relying on
    a specific stdlib dir placement
  * debian/copyright: point to version-specific common license files

  [ Stephane Glondu ]
  * Switching packaging to git

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 28 Feb 2009 22:33:57 +0100

gmetadom (0.2.6-2) unstable; urgency=low

  * debian/patches/
    - add patch gcc-4.3 to build against gcc 4.3 (closes: #441532)
  * fix vcs-svn field to point just above the debian/ dir
  * port debian/copyright to the new copyright format and update
    copyright years
  * bump standards-versions to 3.7.3 (no changes needed)

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 01 May 2008 23:01:33 +0200

gmetadom (0.2.6-1) experimental; urgency=low

  * new upstream release
    - clean some stuff in maintainerclean make target rather than in
      distclean, fix FTBFS if built twice in a row (closes: #442577)
  * add Homepage field to debian/control
  * convert Vcs-* fields to real debian/control fields

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 24 Oct 2007 11:10:52 +0200

gmetadom (0.2.5-3) unstable; urgency=low

  * debian/rules
    - clean using 'clean' target rather than 'distclean', fix FTBFS when
      building twice in a row (closes: #424331)
    - enable generation of ocamldoc documentation (via CDBS)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 04 Sep 2007 16:40:37 +0200

gmetadom (0.2.5-2) experimental; urgency=low

  * rebuild with OCaml 3.10
  * bump debhelper deps and compatibility level to 5
  * debian/control
    - s/Source-Version/binary:Version/

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 16 Jul 2007 15:38:29 +0200

gmetadom (0.2.5-1) unstable; urgency=low

  * new upstream release
    - fixes FTBFS with GCC 4.3 (closes: #417205)
  * debian/watch
    - added watch file

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 08 May 2007 14:24:17 +0200

gmetadom (0.2.4-3) unstable; urgency=low

  * debian/rules
    - use ocaml.mk cdbs class
  * debian/control
    - bumped ocaml-nox build dependencies to >= 3.09.2-7

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  4 Nov 2006 09:40:53 +0100

gmetadom (0.2.4-2) unstable; urgency=low

  * debian/rules
    - removed no longer needed workaround for cdbs + dpatch
    - avoid to create debian/control from debian/control.in on ocamlinit
    - removed from the source package files which are generated at build time
      from the corresponding .in files
  * debian/control.in
    - file removed, no longer needed

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  6 Sep 2006 09:59:11 +0200

gmetadom (0.2.4-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/
    - removed several patches integrated upstream: 03_tests_makefile,
      04_caml_encoding, 07_careful_inst, 11_g++41
  * Rebuilding against the current libxml2 got rid of a spurious -lz
    which can make depending packages FTBFS (closes: #381406)

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  5 Aug 2006 21:09:50 +0200

gmetadom (0.2.3-8) unstable; urgency=low

  * Upload to unstable.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 16 May 2006 20:10:22 +0000

gmetadom (0.2.3-7) experimental; urgency=low

  * Rebuilt against OCaml 3.09.2, bumped deps accordingly.
  * Bumped Standards-Version to 3.7.2 (no changes needed).

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 11 May 2006 22:07:28 +0000

gmetadom (0.2.3-6) unstable; urgency=low

  * Added patch 11_g++41, which fixes FTBFS with g++ 4.1
    (closes: #357600)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 20 Mar 2006 00:21:07 -0500

gmetadom (0.2.3-5) unstable; urgency=low

  * Rebuilt against OCaml 3.09.1, bumped deps accordingly.
  * Added patch 04_caml_encoding from YANG Shouxun which enable saving
    in user-specified encoding (closes: #341071)
  * Added patch 07_careful_inst from Julien Cristau which provide more
    careful Makefile installation target which wont ignore errors
    (closes: #345946)

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  7 Jan 2006 11:56:03 +0100

gmetadom (0.2.3-4) unstable; urgency=low

  * debian/control
    - rebuild/renaming due to changed libstdc++ configuration
      (closes: #339174)

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 17 Nov 2005 10:22:32 +0100

gmetadom (0.2.3-3) unstable; urgency=low

  * Rebuilt with ocaml 3.09
  * debian/*
    - use dpatch
    - no longer hardcoding of ocaml abi version anywhere
  * debian/control
    - bumped standards version

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 13 Nov 2005 11:27:30 +0000

gmetadom (0.2.3-2) unstable; urgency=low

  * src/gdome_caml/test/Makefile.in
    - does not build test.opt if native code compilation is not
      available, fixes FTBS on many archs

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 16 Jul 2005 13:47:42 +0000

gmetadom (0.2.3-1) unstable; urgency=low

  * New upstream release
    - enable static linking of C/OCaml glue code
  * debian/control
    - removed mkdir prehook (upstream make now creates dir properly)
  * no longer patch upstream Makefile in src/gdome_caml (test/ subdirs
    now builds properly)
  * debian/*
    - ABI transition for gcc 4

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  7 Jul 2005 11:56:27 +0200

gmetadom (0.2.2-4) unstable; urgency=low

  * debian/rules
    - uses cdbs
  * debian/control
    - added build-dep on cdbs, bumped debhelper dep accordingly

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 23 May 2005 09:42:51 +0200

gmetadom (0.2.2-3) unstable; urgency=low

  * Rebuilt against ocaml 3.08.3

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 24 Mar 2005 22:26:12 +0100

gmetadom (0.2.2-2) unstable; urgency=low

  * src/gdome_caml/Makefile.in
    - quick and dirty hack to avoid doing make in the "test" dir
      (Closes: Bug#283264)

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  2 Dec 2004 16:51:06 +0100

gmetadom (0.2.2-1) unstable; urgency=low

  * New upstream release
    - fixed a memory leak

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 17 Nov 2004 14:43:00 +0100

gmetadom (0.2.1-4) unstable; urgency=medium

  * debian/control
    - depend on ocaml-base-nox-3.08 instead of ocaml-base-3.08 since
      this package doesn't directly need ocaml X libraries

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 24 Aug 2004 12:26:23 +0200

gmetadom (0.2.1-3) unstable; urgency=low

  * rebuilt with ocaml 3.08
  * debian/control
    - bumped ocaml deps to 3.08
    - bumped standards-version to 3.6.1.1
    - changed ocaml deps to ocaml-nox

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Jul 2004 16:36:56 +0200

gmetadom (0.2.1-2) unstable; urgency=high

  * src/gdome_caml/Makefile.in
    - uses .o PIC objects are from .libs directories
      (Closes: Bug#216627)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 20 Oct 2003 18:43:53 +0200

gmetadom (0.2.1-1) unstable; urgency=low

  * New upstream release
  * Rebuilt with gdome2 0.8.1 (should link against glib2)
  * debian/control
    - removed an anciente ocaml Provides: line
    - fixed dep from libgdome2-ocaml-dev to libgdome2-dev
    - bugfix: added build-dep on pkg-config

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 10 Oct 2003 09:20:53 +0200

gmetadom (0.1.10-5) unstable; urgency=low

  * Rebuilt with ocaml 3.07

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  1 Oct 2003 14:13:27 +0200

gmetadom (0.1.10-4) unstable; urgency=low

  * Rebuilt with ocaml 3.07beta2

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 22 Sep 2003 17:42:56 +0200

gmetadom (0.1.10-3) unstable; urgency=low

  * debian/rules
    - create debian/tmp/usr/lib/ocaml/3.06/{gdome2,stublibs} just before
      makeing upstream "install" Makefile target.
      This is probably the reason why gmetadom is broken on mips.

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  4 Jul 2003 14:59:12 +0200

gmetadom (0.1.10-2) unstable; urgency=low

  * Applied upstream patches that avoid trying to install some ocaml .o
    if ocamlopt compiler isn't available (Closes: #198215)
  * Removed debian TODO (one entry was outdated, the other two belong to
    upstream)

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 25 Jun 2003 17:17:24 +0200

gmetadom (0.1.10-1) unstable; urgency=low

  * New upstream release
    - Cleaned up automake targets, clean no longer remove generated
      files that were already present in the original tarball. This
      hopefully avoid generating flawed package as the last one
      (Closes: #198059)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 20 Jun 2003 10:56:18 +0200

gmetadom (0.1.9-2) unstable; urgency=low

  * bugfix: ship gdome2-cpp-smart.pc
  * debian/rules
    - removed "distclean" target
    - removed dh_undocumented

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 19 Jun 2003 10:38:37 +0200

gmetadom (0.1.9-1) unstable; urgency=low

  * new upstream release
    [ in which missing #include <cassert> are fixed (Closes: Bug#197653) ]
  * debian/control
    - bumped standards-version to 3.5.10
    - added ${misc:Depends}
    - changed section of -dev packages to libdevel
  * debian/rules
    - removed DH_COMPAT in favour of debian/compat
    - use dh_install instead of dh_movefiles

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 18 Jun 2003 12:09:10 +0200

gmetadom (0.1.6-1) unstable; urgency=low

  * minor release 0.1.6:
    - fixed a memory leak in EventListener interface
  * bumped dependencies on libgdome2 to version >= 0.7.4
  * removed autotools garbage from source package
  * removed CVS dir from shipped examples

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  6 Mar 2003 18:37:03 +0100

gmetadom (0.1.5-1) unstable; urgency=low

  * added isSameNode method on the ocaml part to test for physical
    equality of DOM nodes
  * fixed gcc 3.2 build issue (Closes: Bug#175949)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 24 Jan 2003 12:25:41 +0100

gmetadom (0.1.4-3) unstable; urgency=low

  * libgdome2-ocaml-dev now Provides libgdome2-ocaml-dev-0.1.4

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 24 Dec 2002 09:14:24 +0100

gmetadom (0.1.4-2) unstable; urgency=low

  * Libdir transition to /usr/lib/ocaml/3.06
  * Changed depends and build depends to ocaml{,-base}-3.06-1

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 16 Dec 2002 18:50:49 +0100

gmetadom (0.1.4-1) unstable; urgency=low

  * new upstream release
  * moved shared objects in /usr/lib/ocaml/stublibs
  * bumped Standards-Version to 3.5.8
  * removed build-dep on xsltproc, it is useless because 'dist' make
    target prerun xsltproc to generate missing sources
  * removed some useless target from debian/rules
  * use "make clean" to clean the package because debian source package
    ships generated sources and "make distclean" will delete them

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  5 Dec 2002 15:07:42 +0100

gmetadom (0.0.3-7) unstable; urgency=low

  * rebuilt with ocaml 3.06 ((Closes: Bug#158206, Bug#158232)
  * added dep on libgdome2-dev for libgdome2-cpp-smart-dev
    (Closes: Bug#152558)
  * added build-dep on ocaml-findlib
  * switched deps and build-deps to ocaml-3.06 and ocaml-base-3.06
  * upgraded Standards-Version to 3.5.6
  * switched to debhelper 4

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 28 Aug 2002 10:30:36 +0200

gmetadom (0.0.3-6) unstable; urgency=low

  * Fixed C++ problems that inhibit building with g++ 3.0 (in particular on
    arch HPPA)

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  9 Apr 2002 00:17:37 +0200

gmetadom (0.0.3-5) unstable; urgency=low

  * Fixed inclusion of <iostream> in some headers (Closes: #140525).

 -- Stefano Zacchiroli <zack@debian.org>  Mon,  1 Apr 2002 11:50:58 +0200

gmetadom (0.0.3-4) unstable; urgency=low

  * Link also against libstdc++ which is not added by default.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 18:32:17 +0100

gmetadom (0.0.3-3) unstable; urgency=low

  * Added conditional build of ocamlopt targets in (Makefile.am)s
    (Closes: Bug#139493).

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 11:41:41 +0100

gmetadom (0.0.3-2) unstable; urgency=low

  * Changed shlibs handling.

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 13 Mar 2002 19:05:29 +0100

gmetadom (0.0.3-1) unstable; urgency=low

  * Initial Release.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 22 Jan 2002 18:17:18 +0100

